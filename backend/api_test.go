package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http/httptest"
	"os"
	"testing"

	"sevensoupcans.com/backend/vent3/internal/models"
	"sevensoupcans.com/backend/vent3/internal/ventdb"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	exitVal := m.Run()
	os.Exit(exitVal)
}

func getTestUser(ctx context.Context) (*models.VentUser, error) {
	user, err := models.VentUsers(models.VentUserWhere.Username.EQ("test")).OneG(ctx)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func TestJournalGet(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // cancel when we are finished consuming integers

	testUser := ventdb.TestSetup(t, ctx)
	app := CreateTestApp()

	var pwd = new(ventdb.PostWithDetails)
	pwd.UserID = testUser.ID
	pwd.Title = "Test Post"
	pwd.Content = "Content"

	post, err := ventdb.InsertPost(ctx, pwd)
	if err != nil {
		logger.Err.Println(err)
		t.FailNow()
	}

	req := httptest.NewRequest("GET", "/api/v1/journals", nil)
	resp, err := app.Test(req)
	if err != nil {
		t.Fail()
	}

	if resp.StatusCode == 200 {
		body, _ := ioutil.ReadAll(resp.Body)
		var posts []*ventdb.PostWithDetails
		if err := json.Unmarshal(body, &posts); err != nil {
			t.Fail()
		}
		assert.Equal(t, posts[0].ID, post.ID)
		assert.Equal(t, posts[0].Title, post.Title)
		assert.Equal(t, posts[0].Content, post.Content)
	}
	return
}
