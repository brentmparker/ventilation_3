package main

import (
	"github.com/gofiber/fiber/v2"
)

//Route represents the required information for a route for an API call
type Route struct {
	Path    string
	Method  string
	Handler fiber.Handler
}

var (
	accessible []Route
	restricted []Route
	admin      []Route
)

func init() {
	accessible = []Route{
		{
			Path:    "legacy.html",
			Method:  "GET",
			Handler: renderIndex,
		},
		{
			Path:    conf.APIBasePath + "/admins",
			Method:  "GET",
			Handler: getAdmins,
		},
		{
			Path:    conf.APIBasePath + "/journals",
			Method:  "GET",
			Handler: getJournalRoot,
		},
		{
			Path:    conf.APIBasePath + "/lyric",
			Method:  "GET",
			Handler: getLyric,
		},
		{
			Path:    conf.APIBasePath + "/login",
			Method:  "POST",
			Handler: postLogin,
		},
		{
			Path:    conf.APIBasePath + "/currentUser",
			Method:  "GET",
			Handler: getCurrentUser,
		},
		{
			Path:    conf.APIBasePath + "/journals/:jid",
			Method:  "GET",
			Handler: getJournal,
		},
		{
			Path:    conf.APIBasePath + "/journals/:jid/posts/:pid/comments",
			Method:  "GET",
			Handler: getComments,
		},
		{
			Path:    conf.APIBasePath + "/users",
			Method:  "GET",
			Handler: getActiveUsers,
		},
		{
			Path:    conf.APIBasePath + "/users",
			Method:  "POST",
			Handler: newUser,
		},
		{
			Path:    conf.APIBasePath + "/users/confirm",
			Method:  "POST",
			Handler: confirmNewUser,
		},
		{
			Path:    conf.APIBasePath + "/users/confirm/:uuid",
			Method:  "GET",
			Handler: confirmNewUserGet,
		},
		{
			Path:    conf.APIBasePath + "/refresh",
			Method:  "POST",
			Handler: refreshAuthenticatedUser,
		},
	}

	restricted = []Route{
		{
			Path:    conf.APIBasePath + "/journals",
			Method:  "POST",
			Handler: newUser,
		},
		{
			Path:    conf.APIBasePath + "/journals/:jid",
			Method:  "PATCH",
			Handler: updateUser,
		},
		{
			Path:    conf.APIBasePath + "/journals/:jid/pwupdate",
			Method:  "PATCH",
			Handler: updatePassword,
		},
		{
			Path:    conf.APIBasePath + "/journals/:jid",
			Method:  "DELETE",
			Handler: deleteUser,
		},
		{
			Path:    conf.APIBasePath + "/journals/:jid/posts/:pid/comments",
			Method:  "POST",
			Handler: newComment,
		},
		{
			Path:    conf.APIBasePath + "/journals/:jid/posts/:pid/comments/:cid",
			Method:  "PATCH",
			Handler: updateComment,
		},
		{
			Path:    conf.APIBasePath + "/journals/:jid/posts/:pid/comments/:cid",
			Method:  "DELETE",
			Handler: deleteComment,
		},
		{
			Path:    conf.APIBasePath + "/journals/:jid/posts",
			Method:  "POST",
			Handler: newPost,
		},
		{
			Path:    conf.APIBasePath + "/journals/:jid/posts/:pid",
			Method:  "PATCH",
			Handler: updatePost,
		},
		{
			Path:    conf.APIBasePath + "/journals/:jid/posts/:pid",
			Method:  "DELETE",
			Handler: deletePost,
		},
	}
}
