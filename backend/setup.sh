apt-get upgrade
apt-get -y upgrade

cd ~/tmp
wget https://dl.google.com/go/go1.14.6.linux-amd64.tar.gz

tar -xvf go1.14.6.linux-amd64.tar.gz
mv go /usr/local

export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH

source ~/.profile

go get -u github.com/gofiber/fiber
go get -u github.com/gofiber/session
go get -u github.com/gofiber/cors
go get -u github.com/go-sql-driver/mysql
go get -u github.com/gofiber/jwt
go get -u github.com/dgrijalva/jwt-go
go get -u	golang.org/x/oauth2
go get -u golang.org/x/oauth2/google
go get -u google.golang.org/api/gmail/v1
go get -u github.com/jordan-wright/email
go get -tags 'mysql' -u github.com/golang-migrate/migrate/v4/cmd/migrate

#In Mysql database:
GRANT ALL PRIVILEGES on *.* TO 'vent'@'localhost' WITH GRANT OPTIONS;
#with migrate, remove .exe in linux
migrate.exe -database "mysql://vent:vent@tcp(localhost:3306)/vent?parseTime=true&charset=utf8mb4" -path migrations force -1
migrate.exe -database "mysql://vent:vent@tcp(localhost:3306)/vent?parseTime=true&charset=utf8mb4" -path migrations up

#If that doesn't work, undo migration with:  
migrate.exe -database "mysql://vent:vent@tcp(localhost:3306)/vent?parseTime=true&charset=utf8mb4" -path migrations force 1
migrate.exe -database "mysql://vent:vent@tcp(localhost:3306)/vent?parseTime=true&charset=utf8mb4" -path migrations down 

