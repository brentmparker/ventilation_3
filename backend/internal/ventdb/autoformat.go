package ventdb

import (
	"sevensoupcans.com/backend/vent3/internal/models"
)

func PostAutoFormat(p *models.VentPost) {
	if p.Autoformat == true {
		p.Content = autoFormatReplacer.Replace(p.Content)
		p.Title = autoFormatReplacer.Replace(p.Title)
	}
}

func CommentAutoFormat(p *models.VentComment) {
	if p.Autoformat == true {
		p.Content = autoFormatReplacer.Replace(p.Content)
	}
}