package ventdb

import (
	"context"
	//All three added to enable GORM
	_ "github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/mysql"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"sevensoupcans.com/backend/vent3/internal/models"
)

//GetLyric quries and returns a random lyric
func GetLyric(ctx context.Context) (*models.Lyric, error) {
	lyric, err := models.Lyrics(qm.OrderBy("RAND()")).OneG(ctx)
	return lyric, err
}
