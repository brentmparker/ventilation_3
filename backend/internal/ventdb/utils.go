package ventdb

import (
	"context"
	"database/sql"
	"fmt"
	"path/filepath"
	"runtime"
	"testing"

	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"sevensoupcans.com/backend/vent3/config"
	"sevensoupcans.com/backend/vent3/internal/models"
)

var (
	testUser *models.VentUser
)

func TestSetup(t *testing.T, ctx context.Context) *models.VentUser {
	if testUser != nil {
		return testUser
	}
	_, filename, _, _ := runtime.Caller(0)
	path := filepath.Join(filepath.Dir(filename), "..", "..")

	config.ReadConfigFiles([]string{filepath.Join(path, config.ConfigPath), filepath.Join(path, config.ConfigOverridePath), filepath.Join(path, "config.test.json")})

	createDb, err := sql.Open(config.Conf.DBDriver, config.Conf.DSNBase)
	if err != nil {
		config.Log.Err.Println(err)
		t.FailNow()
	}
	res, err := createDb.Exec("CREATE DATABASE IF NOT EXISTS " + config.Conf.DBName)
	if err != nil {
		config.Log.Err.Println(err)
		t.FailNow()
	}
	config.Log.Info.Println(res.RowsAffected())
	if err := createDb.Close(); err != nil {
		config.Log.Err.Println(err)
		t.FailNow()
	}
	InitDatabase(true)
	if _, err := models.NewQuery(qm.SQL(fmt.Sprintf("DELETE FROM %s", models.TableNames.VentUsers))).ExecContext(ctx, boil.GetContextDB()); err != nil {
		config.Log.Err.Println(err)
		t.FailNow()
	}

	if _, err := models.NewQuery(qm.SQL(fmt.Sprintf("DELETE FROM %s", models.TableNames.VentPosts))).ExecContext(ctx, boil.GetContextDB()); err != nil {
		config.Log.Err.Println(err)
		t.FailNow()
	}

	user := models.VentUser{Username: "test", Email: "test@foo.com", Admin: true, Enabled: true, Pending: false}
	rawUser := RawUser{
		VentUser: &user,
		First:    "test",
		Second:   "test",
	}

	testUser = new(models.VentUser)

	testUser, err = InsertUser(ctx, &rawUser)
	if err != nil {
		config.Log.Err.Println(err)
		t.FailNow()
	}

	return testUser
}
