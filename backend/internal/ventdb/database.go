package ventdb

import (
	"database/sql"
	"embed"
	"strings"

	"github.com/volatiletech/sqlboiler/v4/queries"

	"github.com/golang-migrate/migrate/v4/source/iofs"
	"github.com/volatiletech/sqlboiler/v4/boil"
	. "github.com/volatiletech/sqlboiler/v4/queries/qm"
	"sevensoupcans.com/backend/vent3/internal/models"

	"sevensoupcans.com/backend/vent3/config"

	"github.com/golang-migrate/migrate/v4"
	//To enable migration
	_ "github.com/golang-migrate/migrate/v4/source/iofs"

	//because reasons
	_ "crypto/md5"
	_ "crypto/sha1"

	//mysql driver
	_ "github.com/go-sql-driver/mysql"
)

//go:embed migrations/*.sql
var fs embed.FS

var (
	db                 *sql.DB
	logger             *config.Logger
	autoFormatReplacer *strings.Replacer
)

// https://stackoverflow.com/questions/68731784/golang-sqlboiler-append-queries-dynamically
type QueryModSlice []QueryMod

func (s QueryModSlice) Apply(q *queries.Query) {
	Apply(q, s...)
}

//YesNoEnum automatically converts strings "yes" to true and "no" to false
type YesNoEnum bool

//InitDatabase initialize database
func InitDatabase(runMigrations bool) {
	//Setup database here
	var err error
	dsn := config.Conf.GetDSN()
	db, err = sql.Open(config.Conf.DBDriver, dsn)
	if err != nil {
		logger.Err.Fatal(err)
	}

	boil.SetDB(db)
	models.AddVentUserHook(boil.BeforeInsertHook, VentUserBeforeInsert)
	models.AddVentUserHook(boil.BeforeUpsertHook, VentUserBeforeInsert)
	boil.DebugMode = true

	if runMigrations {
		d, err := iofs.New(fs, "migrations")
		if err != nil {
			logger.Err.Fatal(err)
		}
		if config.Conf.DBDriver == "mysql" {
			/*
				dbDriver, err := mysql.WithInstance(db, &mysql.Config{})
				if err != nil {
					logger.Err.Println(err)
				}
			*/
			m, err := migrate.NewWithSourceInstance("iofs", d, config.Conf.GetDSNURL())
			if err != nil {
				logger.Err.Fatal(err)
			}
			err = m.Up()
			if err != nil {
				logger.Info.Println(err)
			}
		}
	}
}

func init() {
	logger = config.Log

	autoFormatReplacer = strings.NewReplacer(
		"\n", "<br />",
		`\'`, "'",
		`\"`, `"`,
	)
}

//GetDatabaseConnection accessor method for database object
func GetDatabaseConnection() *sql.DB {
	return db
}

//UnauthorizedError stuff
type UnauthorizedError struct{}

//Error creates an error string to determine what kind of error is thrown
func (e *UnauthorizedError) Error() string {
	return "Unauthorized"
}

// TxErrHandler reduces boilerplate dealing with transactions
func TxErrHandler(tx *sql.Tx, err error) error {
	if ierr := tx.Rollback(); ierr != nil {
		return ierr
	}
	return err
}
