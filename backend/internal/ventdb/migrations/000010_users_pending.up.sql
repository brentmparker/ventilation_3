ALTER TABLE vent_users MODIFY pending BOOLEAN DEFAULT 1 NOT NULL;

UPDATE vent_comments SET created_at = (SELECT vent_posts.created_at FROM vent_posts WHERE vent_posts.ID = vent_comments.post_id) WHERE created_at IS NULL;

ALTER TABLE vent_comments MODIFY created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE vent_posts MODIFY created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE vent_users MODIFY created_at datetime default CURRENT_TIMESTAMP NOT NULL;

