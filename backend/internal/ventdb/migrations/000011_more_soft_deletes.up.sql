ALTER TABLE vent_tags ADD COLUMN deleted_at timestamp NULL;
ALTER TABLE vent_postdetails ADD COLUMN deleted_at timestamp NULL;
