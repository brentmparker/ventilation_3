ALTER TABLE vent_comments ADD CONSTRAINT comments_user_id_fkey FOREIGN KEY comments_user_id_fkey (userid) REFERENCES vent_users(id) ON DELETE CASCADE;
DELETE FROM vent_comments WHERE postid NOT IN (SELECT id FROM vent_posts);
ALTER TABLE vent_comments ADD CONSTRAINT comments_post_id_fkey FOREIGN KEY comments_post_id_fkey (postid) REFERENCES vent_posts(id) ON DELETE CASCADE;
ALTER TABLE vent_posts ADD CONSTRAINT posts_user_id_fkey FOREIGN KEY posts_user_id_fkey (userid) REFERENCES vent_users(id) ON DELETE CASCADE;
ALTER TABLE vent_postdetails ADD CONSTRAINT postdetails_post_id_fkey FOREIGN KEY postdetails_post_id_fkey (postid) REFERENCES vent_posts(id) ON DELETE CASCADE;
ALTER TABLE vent_tags ADD CONSTRAINT tags_post_id_fkey FOREIGN KEY tags_post_id_fkey (postid) REFERENCES vent_posts(id) ON DELETE CASCADE;
ALTER TABLE vent_moduledata ADD CONSTRAINT moduledata_user_id_fkey FOREIGN KEY moduledata_user_id_fkey (userid) REFERENCES vent_users(id) ON DELETE CASCADE;