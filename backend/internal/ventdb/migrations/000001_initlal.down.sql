DROP TRIGGER IF EXISTS before_insert_vent_users;
ALTER TABLE vent_users DROP COLUMN pending, DROP COLUMN uuid, DROP COLUMN createDate;
