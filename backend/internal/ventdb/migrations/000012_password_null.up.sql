ALTER TABLE vent_users MODIFY COLUMN password varchar(255) NULL DEFAULT NULL;
ALTER TABLE vent_users MODIFY COLUMN override_modules blob NULL DEFAULT NULL;
