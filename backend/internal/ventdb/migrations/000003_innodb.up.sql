ALTER TABLE links ENGINE=InnoDB;
ALTER TABLE lyric ENGINE=InnoDB;
ALTER TABLE themes ENGINE=InnoDB;
ALTER TABLE vent_comments ENGINE=InnoDB;
ALTER TABLE vent_moduleconfig ENGINE=InnoDB;
ALTER TABLE vent_moduledata ENGINE=InnoDB;
ALTER TABLE vent_modules ENGINE=InnoDB;
ALTER TABLE vent_postdetails ENGINE=InnoDB;
ALTER TABLE vent_posts ENGINE=InnoDB;
ALTER TABLE vent_settings ENGINE=InnoDB;
ALTER TABLE vent_tags ENGINE=InnoDB;
ALTER TABLE vent_users ENGINE=InnoDB;

