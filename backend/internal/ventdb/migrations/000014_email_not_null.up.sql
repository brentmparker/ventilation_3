UPDATE vent_users SET email = '' WHERE email IS NULL;

ALTER TABLE vent_users MODIFY COLUMN email text NOT NULL;
