ALTER TABLE vent_users ADD COLUMN admin_bool BOOLEAN DEFAULT FALSE NOT NULL;
UPDATE vent_users SET vent_users.admin_bool = TRUE WHERE admin = 'yes';
ALTER TABLE vent_users DROP COLUMN admin;
ALTER TABLE vent_users RENAME COLUMN admin_bool TO admin;

ALTER TABLE vent_posts ADD COLUMN locked_bool BOOLEAN DEFAULT FALSE NOT NULL;
UPDATE vent_posts SET vent_posts.locked_bool = TRUE WHERE locked = 'yes';
ALTER TABLE vent_posts DROP COLUMN locked;
ALTER TABLE vent_posts RENAME COLUMN locked_bool TO locked;

ALTER TABLE vent_posts ADD COLUMN autoformat_bool BOOLEAN DEFAULT TRUE NOT NULL;
UPDATE vent_posts SET vent_posts.autoformat_bool = TRUE WHERE autoformat = 'yes';
ALTER TABLE vent_posts DROP COLUMN autoformat;
ALTER TABLE vent_posts RENAME COLUMN autoformat_bool TO autoformat;

ALTER TABLE vent_comments ADD COLUMN autoformat_bool BOOLEAN DEFAULT TRUE NOT NULL;
UPDATE vent_comments SET vent_comments.autoformat_bool = TRUE WHERE autoformat = 'yes';
ALTER TABLE vent_comments DROP COLUMN autoformat;
ALTER TABLE vent_comments RENAME COLUMN autoformat_bool TO autoformat;