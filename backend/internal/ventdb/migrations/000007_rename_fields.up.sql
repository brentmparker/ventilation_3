ALTER TABLE vent_posts RENAME COLUMN post TO content;
ALTER TABLE vent_posts RENAME COLUMN topic TO title;
ALTER TABLE vent_posts RENAME COLUMN dateposted TO created_at;
ALTER TABLE vent_posts RENAME COLUMN userid TO user_id;

ALTER TABLE vent_posts ADD COLUMN updated_at timestamp NULL;
ALTER TABLE vent_posts ADD COLUMN deleted_at timestamp NULL;

ALTER TABLE vent_comments RENAME COLUMN post TO content;
ALTER TABLE vent_comments RENAME COLUMN userid TO user_id;
ALTER TABLE vent_comments RENAME COLUMN postid TO post_id;

ALTER TABLE vent_comments ADD COLUMN created_at timestamp NULL;
ALTER TABLE vent_comments ADD COLUMN updated_at timestamp NULL;
ALTER TABLE vent_comments ADD COLUMN deleted_at timestamp NULL;

ALTER TABLE vent_moduledata RENAME COLUMN userid TO user_id;

ALTER TABLE vent_users RENAME COLUMN createDate TO created_at;
ALTER TABLE vent_users ADD COLUMN updated_at timestamp NULL;
ALTER TABLE vent_users ADD COLUMN deleted_at timestamp NULL;
