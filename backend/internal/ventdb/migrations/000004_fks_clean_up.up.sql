# There is no going back!

DELETE FROM vent_comments WHERE userid NOT IN (SELECT id FROM vent_users);
DELETE FROM vent_comments WHERE postid NOT IN (SELECT id FROM vent_posts);
DELETE FROM vent_posts WHERE userid NOT IN (SELECT id FROM vent_users);
DELETE FROM vent_postdetails WHERE postid NOT IN (SELECT id FROM vent_posts);
DELETE FROM vent_tags WHERE postid NOT IN (SELECT id FROM vent_posts);
DELETE FROM vent_moduledata WHERE userid NOT IN (SELECT id FROM vent_users);
