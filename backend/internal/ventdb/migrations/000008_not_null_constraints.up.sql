ALTER TABLE vent_users MODIFY username varchar(255) NOT NULL;
ALTER TABLE vent_users MODIFY password varchar(255) NOT NULL;
ALTER TABLE vent_users MODIFY uuid varchar(36) NOT NULL;

ALTER TABLE vent_posts MODIFY title text NOT NULL;
ALTER TABLE vent_posts MODIFY content longtext NOT NULL;
ALTER TABLE vent_posts MODIFY user_id int NOT NULL;
ALTER TABLE vent_comments MODIFY post_id int NOT NULL;
ALTER TABLE vent_comments MODIFY user_id int NOT NULL;
ALTER TABLE vent_comments MODIFY content mediumtext NOT NULL;

ALTER TABLE vent_postdetails RENAME COLUMN postid TO post_id;

ALTER TABLE vent_postdetails MODIFY post_id int NOT NULL;
ALTER TABLE vent_postdetails MODIFY name text NOT NULL;
ALTER TABLE vent_postdetails MODIFY value text NOT NULL;

ALTER TABLE vent_tags RENAME COLUMN postid TO post_id;
ALTER TABLE vent_tags MODIFY post_id int NOT NULL;
ALTER TABLE vent_tags MODIFY tag varchar(255) NOT NULL;

DELETE FROM vent_tags WHERE tag = '';
