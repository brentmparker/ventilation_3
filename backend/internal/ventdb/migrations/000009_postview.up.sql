CREATE OR REPLACE VIEW vent_postview AS
SELECT p.*,
       COUNT(DISTINCT vc.ID) AS                                                               comment_count,
       CASE
           WHEN vt.post_id is NULL THEN NULL
           ELSE CAST(CONCAT('[', GROUP_CONCAT(DISTINCT JSON_OBJECT('id', vt.ID, 'tag', vt.tag)), ']') AS JSON) END tags,
       CASE
           WHEN vp.post_id is NULL THEN NULL
           ELSE CAST(CONCAT('[', GROUP_CONCAT(DISTINCT JSON_OBJECT('id', vp.ID, 'name', vp.name, 'value', vp.value)),
                            ']') AS JSON) END                                                 details,
       vu.admin, vu.username
FROM vent_posts AS p
         LEFT OUTER JOIN vent_comments vc on p.ID = vc.post_id
         LEFT OUTER JOIN vent_tags vt on vc.post_id = vt.post_id
         LEFT OUTER JOIN vent_postdetails vp on vc.post_id = vp.post_id
         LEFT OUTER JOIN vent_users vu on p.user_id = vu.ID
GROUP BY p.ID, p.created_at
ORDER BY p.created_at DESC;

