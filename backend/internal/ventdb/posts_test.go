package ventdb

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"

	"sevensoupcans.com/backend/vent3/internal/models"
)

func TestValidatePost(t *testing.T) {
	pwd := PostWithDetails{}
	err := ValidatePost(&pwd)
	require.NotNil(t, err, err)

	pwd.UserID = 1
	err = ValidatePost(&pwd)
	require.NotNil(t, err, err)

	pwd.Title = "foo"
	err = ValidatePost(&pwd)
	require.Nil(t, err, err)

	pwd.UserID = 0
	err = ValidatePost(&pwd)
	require.NotNil(t, err, err)

	pwd.User = new(models.VentUser)
	err = ValidatePost(&pwd)
	require.NotNil(t, err, err)

	pwd.User.ID = 1
	err = ValidatePost(&pwd)
	require.Nil(t, err, err)

	pwd.Details = append(pwd.Details, new(models.VentPostdetail))
	err = ValidatePost(&pwd)
	require.NotNil(t, err, err)

	pwd.Details[0].Name = "foo"
	err = ValidatePost(&pwd)
	require.Nil(t, err, err)

	pwd.Tags = append(pwd.Tags, new(models.VentTag))
	err = ValidatePost(&pwd)
	require.NotNil(t, err, err)

	pwd.Tags[0].Tag = "bar"
	err = ValidatePost(&pwd)
	require.Nil(t, err, err)
}

func TestPostInsertBasic(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	user := TestSetup(t, ctx)


	pwd := PostWithDetails{User: user}
	pwd.UserID = user.ID

	post, err := InsertPost(ctx, &pwd)
	require.NotNil(t, err, err)
	require.Nil(t, post, "post is not nil")

	pwd.ID = 0
	pwd.Title = "foo"
	post, err = InsertPost(ctx, &pwd)
	require.Nil(t, err, err)
	require.Equal(t, post.UserID, user.ID)
	require.Greater(t, post.ID, 0)
	require.Greater(t, post.CreatedAt.Unix(), int64(0))
}

func TestPostInsertDetails(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	user := TestSetup(t, ctx)


	pwd := PostWithDetails{User: user}
	pwd.UserID = user.ID
	pwd.Title = "foo"
	pwd.Details = append(pwd.Details, new(models.VentPostdetail))

	post, err := InsertPost(ctx, &pwd)
	require.NotNil(t, err, err)
	require.Nil(t, post, "post is not nil")

	pwd.ID = 0
	pwd.Details[0].Value = "bar"
	post, err = InsertPost(ctx, &pwd)
	require.NotNil(t, err, err)
	require.Nil(t, post, "post is not nil")

	pwd.ID = 0
	pwd.Details[0].Name = "foo"
	post, err = InsertPost(ctx, &pwd)
	require.Nil(t, err, err)
	require.Greater(t, post.ID, 0)
	require.Greater(t, post.Details[0].ID, 0)
}

func TestPostInsertTags(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	user := TestSetup(t, ctx)

	pwd := PostWithDetails{User: user}
	pwd.UserID = user.ID
	pwd.Title = "foo"
	pwd.Tags = append(pwd.Tags, new(models.VentTag))

	post, err := InsertPost(ctx, &pwd)
	require.NotNil(t, err, err)
	require.Nil(t, post, "post is not nil")

	pwd.ID = 0
	pwd.Tags[0].Tag = "foo"
	post, err = InsertPost(ctx, &pwd)
	require.Nil(t, err, err)
	require.Greater(t, post.ID, 0)
	require.Greater(t, post.Tags[0].ID, 0)
}
