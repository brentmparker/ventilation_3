package ventdb

import (
	"testing"

	"github.com/stretchr/testify/require"

	"sevensoupcans.com/backend/vent3/internal/models"
)

func TestValidateUser(t *testing.T) {
	// empty email and username
	user := new(models.VentUser)
	err := ValidateUser(user)
	require.NotNil(t, err, err)

	// valid username, empty email
	user.Username = "foo"
	err = ValidateUser(user)
	require.NotNil(t, err, err)

	// valid username, invalid email
	user.Email = "foo"
	err = ValidateUser(user)
	require.NotNil(t, err, err)

	// valid username, valid email
	user.Email = "foo@bar"
	err = ValidateUser(user)
	require.Nil(t, err, err)

	// invalid username, valid email
	user.Username = ""
	err = ValidateUser(user)
	require.NotNil(t, err, err)
}
