package ventdb

import (
	"context"
	"crypto/md5"
	"crypto/sha1"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	. "github.com/volatiletech/sqlboiler/v4/queries/qm"
	"sevensoupcans.com/backend/vent3/internal/models"
)

//UserView encapsulates the data used on the Active Users view
type UserView struct {
	ID uint `boil:"id" json:"id,omitempty"`

	Username string `boil:"id" json:"username,omitempty"`

	LastPost time.Time `boil:"last_post" json:"lastPost,omitempty"`

	PostTotal int `boil:"post_total" json:"postTotal,omitempty"`
}

//Credentials username and password credentials received for login
//Could also be used for creating a new user
type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

//Potentially replaced with RawUser??
type PasswordUpdate struct {
	Original string `json:"original"`
	First    string `json:"first"`
	Second   string `json:"second"`
}

// Used to create a new user including raw password and confirm password
// Original is unused when creating a new user
// It is only there in case this is used for password reset
type RawUser struct {
	*models.VentUser
	Original string `json:"original,omitempty"`
	First    string `json:"first"`
	Second   string `json:"second"`
}

func VentUserBeforeInsert(ctx context.Context, exec boil.ContextExecutor, u *models.VentUser) error {
	if u.ID == 0 && u.UUID == "" {
		u.UUID = uuid.New().String()
	}
	return nil
}

//GetAdminUsers queries and returns a list of users with admin privileges
func GetAdminUsers(ctx context.Context) ([]*models.VentUser, error) {
	users, err := models.VentUsers(models.VentUserWhere.Admin.EQ(true)).AllG(ctx)
	return users, err
}

//Exists checks to see if a username or email exists in the database
//Returns "true" if it exists, "false" it does not
//Returns strings because that is what frontend api is expecting
func Exists(ctx context.Context, username string, email string) error {
	var count int64
	var err error
	if count, err = models.VentUsers(qm.Where(fmt.Sprintf("LOWER(%v) = LOWER( ? ) OR LOWER(%v) = LOWER( ? )", models.VentUserColumns.Email, models.VentUserColumns.Username), email, username)).CountG(ctx); err != nil {
		return err
	}
	if count > 0 {
		return errors.New("user already exists")
	}
	return nil
}

//ConfirmNewUser takes a uuid and sets vent_users.pending to false
//This function confirms that a new user has validated their
//email address
func ConfirmNewUser(ctx context.Context, uuid string) (bool, error) {
	user, err := models.VentUsers(models.VentUserWhere.UUID.EQ(uuid)).OneG(ctx)
	if err != nil {
		return false, err
	}
	user.Pending = false
	result, err := user.UpdateG(ctx, boil.Whitelist(models.VentUserColumns.Pending, models.VentUserColumns.UpdatedAt))
	return result == 1, err
}

//GetUserByID selects the important information about a single user
func GetUserByID(userID int, ctx context.Context) (*models.VentUser, error) {
	obj, err := models.VentUsers(Where("ID = ? ", userID)).OneG(ctx)
	return obj, err
}


//ValidateUser checks the fields of a user model to ensure their integrity
func ValidateUser(user *models.VentUser) error {
	if len(user.Username) == 0 {
		return errors.New("username cannot be empty")
	}
	if !strings.Contains(user.Email, "@") {
		return errors.New("email is not valid")
	}
	return nil
}

//InsertUser inserts a new user into the database
func InsertUser(ctx context.Context, user *RawUser) (*models.VentUser, error) {
	// Final checks for server-side security
	// Make sure passwords match. This should also be checked
	// on the frontend, but is here for extra security
	if user.First != user.Second {
		return nil, errors.New("passwords do not match")
	}
	if err := ValidateUser(user.VentUser); err != nil {
		return nil, err
	}

	if err := Exists(ctx, user.Username, user.Email); err != nil {
		return nil, err
	}

	//Generate password
	finalPass := GeneratePassword(user.First)

	tx, err := boil.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	if err := user.InsertG(ctx, boil.Greylist(models.VentUserColumns.Pending)); err != nil {
		return nil, TxErrHandler(tx, err)
	}

	// TODO: Check that update was successful

	result, err := queries.RawG(fmt.Sprintf("UPDATE %s SET password = ? WHERE %s = ?", models.TableNames.VentUsers, models.VentUserColumns.ID), finalPass, user.ID).ExecContext(ctx, boil.GetContextDB())
	if err != nil {
		return nil, TxErrHandler(tx, err)
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return nil, TxErrHandler(tx, err)
	}
	if rows != 1 {
		return nil, TxErrHandler(tx, errors.New("error creating user"))
	}

	if err := tx.Commit(); err != nil {
		return nil, TxErrHandler(tx, err)
	}

	//Return user so user can go through authentication
	return user.VentUser, nil
}

//UpdateUser updates a user existing in the database
func UpdateUser(ctx context.Context, user *models.VentUser) (*models.VentUser, error) {
	if err := ValidateUser(user); err != nil {
		return nil, err
	}
	tx, err := boil.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	// Is there anything else we should blacklist when updating??
	rows, err := user.UpdateG(ctx, boil.Blacklist(models.VentUserColumns.Enabled, models.VentUserColumns.Pending, models.VentUserColumns.Admin, models.VentUserColumns.UUID, models.VentUserColumns.Email))
	if err != nil {
		return nil, TxErrHandler(tx, err)
	}
	if rows != 1 {
		return nil, TxErrHandler(tx, errors.New("unable to update user"))
	}
	return user, nil
}

func UpdatePassword(ctx context.Context, user *RawUser) (bool, error) {
	if user.First != user.Second {
		return false, errors.New("passwords do not match")
	}
	checkUser, err := Authenticate(ctx, Credentials{Username: user.Username, Password: user.Original})
	if err != nil {
		return false, err
	}
	if checkUser.ID != user.ID {
		return false, errors.New("unable to update password")
	}
	finalPass := GeneratePassword(user.First)

	tx, err := boil.BeginTx(ctx, nil)
	if err != nil {
		return false, err
	}

	result, err := models.NewQuery(qm.SQL(fmt.Sprintf("UPDATE %v SET password = ? WHERE %v = ?", models.TableNames.VentUsers, models.VentUserColumns.ID), finalPass, user.ID)).ExecContext(ctx, boil.GetContextDB())
	if err != nil {
		return false, TxErrHandler(tx, err)
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return false, TxErrHandler(tx, err)
	}
	if rows != 1 {
		return false, TxErrHandler(tx, errors.New("unable to update password, invalid row count"))
	}
	if err := tx.Commit(); err != nil {
		return false, TxErrHandler(tx, err)
	}
	return true, nil
}

//GeneratePassword generates hashed password
func GeneratePassword(password string) string {
	passBytes := []byte(password)
	passMd5 := fmt.Sprintf("%x", md5.Sum(passBytes))
	passSha1 := fmt.Sprintf("%x", sha1.Sum([]byte(passMd5)))
	combinedPass := passSha1 + passMd5
	combinedBytes := []byte(combinedPass)
	return fmt.Sprintf("%x", md5.Sum(combinedBytes))
}

//Authenticate processes authentication and modifies the fiber context
func Authenticate(ctx context.Context, creds Credentials) (*models.VentUser, error) {
	finalPass := GeneratePassword(creds.Password)

	// value to test the no rows use case
	return models.VentUsers(
		Select(
			models.VentUserColumns.ID,
			models.VentUserColumns.Username,
			models.VentUserColumns.Visibleemail,
			models.VentUserColumns.Email,
			models.VentUserColumns.Firstname,
			models.VentUserColumns.Lastname,
			models.VentUserColumns.Admin),
		Where(fmt.Sprintf("%v = ? AND password = ? AND %v = TRUE AND %v = FALSE",
			models.VentUserColumns.Username,
			models.VentUserColumns.Enabled,
			models.VentUserColumns.Pending),
			creds.Username,
			finalPass)).OneG(ctx)
}

//GetActiveUsers Selects a list of active users with summary data
func GetActiveUsers(ctx context.Context) ([]*UserView, error) {
	var activeUsers []*UserView

	err := models.NewQuery(
		Select(
			models.VentUserTableColumns.ID,
			models.VentUserTableColumns.Username,
			fmt.Sprintf("COUNT(%s) AS post_total", models.VentPostTableColumns.ID),
			fmt.Sprintf("(SELECT %s FROM %s WHERE %s = %s ORDER BY %s DESC LIMIT 1) AS last_post", models.VentPostTableColumns.CreatedAt, models.TableNames.VentPosts, models.VentPostTableColumns.UserID, models.VentUserTableColumns.ID, models.VentPostTableColumns.CreatedAt)),
		From(models.TableNames.VentUsers),
		LeftOuterJoin(fmt.Sprintf("%s ON %s = %s", models.TableNames.VentPosts, models.VentPostTableColumns.UserID, models.VentUserTableColumns.ID)),
		OrderBy("last_post DESC"),
	).BindG(ctx, activeUsers)

	return activeUsers, err
}
