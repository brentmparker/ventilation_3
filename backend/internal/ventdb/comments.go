package ventdb

import (
	"context"
	"errors"
	"time"

	"github.com/volatiletech/sqlboiler/v4/boil"

	. "github.com/volatiletech/sqlboiler/v4/queries/qm"
	"sevensoupcans.com/backend/vent3/internal/models"
)

type CommentWithUser struct {
	models.VentComment
	User          *models.VentUser `json:"user"`
	AutoFormatted bool             `json:"-"`
}

func (p *CommentWithUser) AutoFormat() {
	if p.Autoformat == true && !p.AutoFormatted {
		p.Content = autoFormatReplacer.Replace(p.Content)
		p.Autoformat = true
	}
}

func AddUserToCommentSlice(comments []*models.VentComment) []*CommentWithUser {
	var convComments []*CommentWithUser
	for _, c := range comments {
		convComments = append(convComments, NewCommentWithUser(c))
	}
	return convComments
}

func NewCommentWithUser(comment *models.VentComment) *CommentWithUser {
	var newC = CommentWithUser{VentComment: *comment, User: comment.R.User}
	newC.AutoFormat()
	return &newC
}

//FindComment selects a single comment from the database by id
func FindComment(id int, ctx context.Context) (*models.VentComment, error) {
	obj, err := models.VentComments(Load(models.VentCommentRels.User), models.VentCommentWhere.ID.EQ(id), models.VentCommentWhere.DeletedAt.IsNull()).OneG(ctx)
	return obj, err
}

//FindCommentWithUser selects a single comment from the database by id
func FindCommentWithUser(ctx context.Context, id int) (*CommentWithUser, error) {
	comment, err := FindComment(id, ctx)
	if err != nil {
		return nil, err
	}
	return NewCommentWithUser(comment), err
}

//GetCommentsByPostID queries and returns a subset of comments for a given post
func GetCommentsByPostID(jid int, pid int, ctx context.Context) ([]*CommentWithUser, error) {
	postComments, err := models.VentComments(
		Load(models.VentCommentRels.User),
		models.VentCommentWhere.DeletedAt.IsNull(),
		models.VentCommentWhere.PostID.EQ(pid),
	).AllG(ctx)
	return AddUserToCommentSlice(postComments), err
}

func checkForPostExistence(ctx context.Context, comment *models.VentComment) error {
	exists, err := models.VentPosts(models.VentPostWhere.ID.EQ(comment.PostID)).ExistsG(ctx)
	if err != nil {
		return err
	}
	if !exists {
		return errors.New("post does not exist")
	}
	return nil
}

//InsertComment inserts a new comment into the database
func InsertComment(ctx context.Context, comment *CommentWithUser) (*CommentWithUser, error) {
	if err := checkForPostExistence(ctx, &comment.VentComment); err != nil {
		return nil, err
	}
	comment.UpdatedAt.Valid = false
	comment.DeletedAt.Valid = false

	if err := comment.InsertG(ctx, boil.Infer()); err != nil {
		return nil, err
	}

	return FindCommentWithUser(ctx, comment.ID)
}

//UpdateComment updates a comment existing in the database
func UpdateComment(ctx context.Context, comment *models.VentComment) (*CommentWithUser, error) {
	if err := checkForPostExistence(ctx, comment); err != nil {
		return nil, err
	}

	result, err := comment.UpdateG(ctx, boil.Whitelist(models.VentCommentColumns.Content, models.VentCommentColumns.UpdatedAt, models.VentCommentColumns.Autoformat))
	if err != nil {
		return nil, err
	}
	if result != 1 {
		return nil, errors.New("could not update comment, comment was not found")
	}

	return FindCommentWithUser(ctx, comment.ID)
}

//DeleteComment Deletes a comment from the database
func DeleteComment(ctx context.Context, comment *models.VentComment) error {
	now := time.Now()
	if err := comment.DeletedAt.Scan(now); err != nil {
		return err
	}
	_, err := comment.UpdateG(ctx, boil.Whitelist(models.VentCommentColumns.DeletedAt))
	return err
}
