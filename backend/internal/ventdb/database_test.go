package ventdb

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/volatiletech/sqlboiler/v4/boil"
	"sevensoupcans.com/backend/vent3/internal/models"
)

func TestMain(m *testing.M) {
	exitVal := m.Run()
	os.Exit(exitVal)
}

func TestUserCreate(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	user := TestSetup(t, ctx)
	require.NotEmpty(t, user.UUID, "uuid cannot by empty")
}

func TestUserAuth(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	user := TestSetup(t, ctx)
	creds := Credentials{
		Username: "test",
		Password: "test",
	}
	checkUser, err := Authenticate(ctx, creds)
	require.Nil(t, err, err)
	require.Equal(t, user.ID, checkUser.ID)

	user.Pending = true
	_, err = user.UpdateG(ctx, boil.Whitelist(models.VentUserColumns.Pending))
	require.Nil(t, err, err)
	checkUser, err = Authenticate(ctx, creds)
	require.NotNil(t, err, err)

	user.Pending = false
	user.Enabled = false
	_, err = user.UpdateG(ctx, boil.Whitelist(models.VentUserColumns.Pending, models.VentUserColumns.Enabled))
	require.Nil(t, err, err)
	checkUser, err = Authenticate(ctx, creds)
	require.NotNil(t, err, err)
}
