package ventdb

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/volatiletech/sqlboiler/v4/boil"
	. "github.com/volatiletech/sqlboiler/v4/queries/qm"
	"sevensoupcans.com/backend/vent3/internal/models"
)

type PostWithDetails struct {
	models.VentPost
	CommentCount    int64                    `json:"comment_count"`
	User            *models.VentUser         `json:"user"`
	Tags            []*models.VentTag        `json:"tags"`
	Details         []*models.VentPostdetail `json:"details"`
	AutoFormatted   bool                     `json:"-"`
	CommentsCounted bool                     `json:"-"`
}

type PostCommentCount struct {
	PostId int   `boil:"post_id"`
	Count  int64 `boil:"count"`
}

func AddDetailsToPostSlice(posts []*models.VentPost, ctx context.Context) ([]*PostWithDetails, error) {
	var postIds []int
	for _, p := range posts {
		postIds = append(postIds, p.ID)
	}
	// Do a raw query to get the combined comment counts from the database
	q := models.NewQuery(
		Select(fmt.Sprintf("COUNT(%s) AS count", models.VentCommentColumns.ID), models.VentCommentColumns.PostID),
		From(models.TableNames.VentComments),
		models.VentCommentWhere.PostID.IN(postIds),
		models.VentCommentWhere.DeletedAt.IsNull(),
		GroupBy(models.VentCommentColumns.PostID))
	// Bind to our raw query struct
	var commentCounts []*PostCommentCount
	if err := q.BindG(ctx, &commentCounts); err != nil {
		return nil, err
	}
	// Convert it to a map for easy access
	countMap := make(map[int]int64)
	for _, c := range commentCounts {
		countMap[c.PostId] = c.Count
	}

	var convPosts []*PostWithDetails
	for _, p := range posts {
		newP, err := NewPostWithDetails(p, countMap[p.ID], ctx)
		if err != nil {
			return convPosts, err
		}
		convPosts = append(convPosts, newP)
	}
	return convPosts, nil
}

func (p *PostWithDetails) AddDetails(ctx context.Context) error {
	p.AutoFormat()
	if p.R.Details != nil {
		p.Details = p.R.Details
	}
	if p.R.PostVentTags != nil {
		p.Tags = p.R.PostVentTags
	}
	var err error
	if !p.CommentsCounted {
		p.CommentCount, err = p.Comments().CountG(ctx)
	}
	return err
}

func (p *PostWithDetails) AutoFormat() {
	if p.Autoformat == true && !p.AutoFormatted {
		p.Content = autoFormatReplacer.Replace(p.Content)
		p.Title = autoFormatReplacer.Replace(p.Title)
		p.Autoformat = true
	}
}

func (p *PostWithDetails) ReloadG(ctx context.Context) error {
	post, err := models.VentPosts(Load(models.VentPostRels.User), Load(models.VentPostRels.PostVentTags), Load(models.VentPostRels.Details), models.VentPostWhere.ID.EQ(p.ID)).OneG(ctx)
	if err != nil {
		return err
	}
	newP, err := NewPostWithDetails(post, 0, ctx)
	if err != nil {
		return err
	}
	if err := newP.AddDetails(ctx); err != nil {
		return err
	}
	*p = *newP
	return nil
}

func NewPostWithDetails(post *models.VentPost, commentCount int64, ctx context.Context) (*PostWithDetails, error) {
	var newP = PostWithDetails{VentPost: *post, User: post.R.User, CommentCount: commentCount, CommentsCounted: commentCount >= 0}
	err := newP.AddDetails(ctx)
	return &newP, err
}

//GetRecentPosts retrieves the most recent cnt journal posts posted by anyone
func GetRecentPosts(ctx context.Context, offset int) ([]*PostWithDetails, error) {
	posts, err := models.VentPosts(Load(models.VentPostRels.User), Limit(10), Offset(offset), models.VentPostWhere.DeletedAt.IsNull(), OrderBy(fmt.Sprintf("%s DESC", models.VentPostColumns.CreatedAt))).AllG(ctx)
	if err != nil {
		return nil, err
	}
	convPosts, err := AddDetailsToPostSlice(posts, ctx)
	return convPosts, err
}

//GetJournalPosts retrieves all the posts for a given journal
// If `jid` is 0 then
func GetJournalPosts(jid int, offset int, limit int, ctx context.Context) ([]*PostWithDetails, error) {
	mods := QueryModSlice{
		Load(models.VentPostRels.User), Load(models.VentPostRels.PostVentTags), Load(models.VentPostRels.Details), Offset(offset), Limit(limit), models.VentPostWhere.DeletedAt.IsNull(), OrderBy("created_at DESC"),
	}
	if jid > 0 {
		mods = append(mods, models.VentPostWhere.UserID.EQ(jid))
	} else {
		mods = append(mods, InnerJoin(fmt.Sprintf("%s u on u.%s = %s", models.TableNames.VentUsers, models.VentUserColumns.ID, models.VentPostColumns.UserID)), Where(fmt.Sprintf("u.%s = TRUE", models.VentUserColumns.Admin)))
	}
	posts, err := models.VentPosts(mods).AllG(ctx)
	if err != nil {
		return nil, err
	}
	convPosts, err := AddDetailsToPostSlice(posts, ctx)
	return convPosts, err
}

func insertDetailsAndTags(ctx context.Context, post *PostWithDetails, tx *sql.Tx) error {
	for _, detail := range post.Details {
		// Require a name at least, value could be empty
		if len(detail.Name) == 0 {
			return errors.New("detail name cannot be empty")
		}
		detail.ID = 0
		detail.PostID = post.ID
		if err := detail.Insert(ctx, tx, boil.Infer()); err != nil {
			return TxErrHandler(tx, err)
		}
	}
	for _, tag := range post.Tags {
		if len(tag.Tag) == 0 {
			return errors.New("tag cannot be empty")
		}
		tag.ID = 0
		tag.PostID = post.ID
		if err := tag.Insert(ctx, tx, boil.Infer()); err != nil {
			return TxErrHandler(tx, err)
		}
	}
	return nil
}

func ValidatePost(post *PostWithDetails) error {
	if post.UserID <= 0 {
		if post.User == nil || post.User.ID <= 0 {
			return errors.New("UserID must be >= 0")
		}
	}

	if len(post.Title) == 0 {
		return errors.New("title cannot be empty")
	}

	for _, detail := range post.Details {
		// Require a name at least, value could be empty
		if len(detail.Name) == 0 {
			return errors.New("detail name cannot be empty")
		}
	}

	for _, tag := range post.Tags {
		if len(tag.Tag) == 0 {
			return errors.New("tag cannot be empty")
		}
	}
	return nil
}

//InsertPost does what you think it does
func InsertPost(ctx context.Context, post *PostWithDetails) (*PostWithDetails, error) {
	/*
		TODO: Is this really the way to do it?
		Does SQLBoiler have some way to handle automatically inserting
		dependent records?
	*/
	if err := ValidatePost(post); err != nil {
		return nil, err
	}

	tx, err := boil.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}

	post.DeletedAt.Valid = false
	post.UpdatedAt.Valid = false

	if err := post.Insert(ctx, tx, boil.Infer()); err != nil {
		return nil, TxErrHandler(tx, err)
	}
	if err := insertDetailsAndTags(ctx, post, tx); err != nil {
		return nil, err
	}

	err = tx.Commit()
	if err != nil {
		return nil, TxErrHandler(tx, err)
	}

	err = post.ReloadG(ctx)
	return post, err
}

//UpdatePost updates a post
func UpdatePost(ctx context.Context, post *PostWithDetails) (*PostWithDetails, error) {

	if err := ValidatePost(post); err != nil {
		return nil, err
	}

	// Create a transaction so we can undo any incomplete changes
	tx, err := boil.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}

	// If we are editing through the update path, then we are assuming an undelete
	post.DeletedAt.Valid = false

	rows, err := post.Update(ctx, tx, boil.Infer())
	if err != nil {
		return nil, TxErrHandler(tx, err)
	}
	if rows == 0 {
		return nil, TxErrHandler(tx, errors.New("updated 0 posts"))
	}
	// Just nuke existing details and tags
	_, err = models.VentPostdetails(models.VentPostdetailWhere.PostID.EQ(post.ID)).DeleteAllG(ctx)
	_, err = models.VentTags(models.VentTagWhere.PostID.EQ(post.ID)).DeleteAllG(ctx)
	// And re-insert
	if err := insertDetailsAndTags(ctx, post, tx); err != nil {
		return nil, err
	}

	err = tx.Commit()
	if err != nil {
		return nil, TxErrHandler(tx, err)
	}

	err = post.ReloadG(ctx)
	return post, err
}

//DeletePost deletes a post
func DeletePost(ctx context.Context, postID int) error {
	post, err := models.VentPosts(Load(models.VentPostRels.PostVentTags), Load(models.VentPostRels.Details), models.VentPostWhere.ID.EQ(postID)).OneG(ctx)
	if err != nil {
		return err
	}
	// Create a transaction so we can undo any incomplete changes
	tx, err := boil.BeginTx(ctx, nil)
	if err != nil {
		return TxErrHandler(tx, err)
	}

	now := time.Now()
	if err := post.DeletedAt.Scan(now); err != nil {
		return TxErrHandler(tx, err)
	}
	_, err = post.UpdateG(ctx, boil.Whitelist(models.VentPostColumns.DeletedAt))
	if err != nil {
		return TxErrHandler(tx, err)
	}
	_, err = models.VentTags(models.VentTagWhere.PostID.EQ(post.ID)).UpdateAllG(ctx, models.M{models.VentTagColumns.DeletedAt: now})
	if err != nil {
		return TxErrHandler(tx, err)
	}
	_, err = models.VentPostdetails(models.VentPostdetailWhere.PostID.EQ(post.ID)).UpdateAllG(ctx, models.M{models.VentPostdetailColumns.DeletedAt: now})
	if err != nil {
		return TxErrHandler(tx, err)
	}

	return tx.Commit()
}
