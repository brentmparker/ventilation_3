package middleware

import (
	"errors"
	"fmt"
	"time"

	"github.com/volatiletech/null/v8"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"

	"github.com/google/uuid"
	"github.com/volatiletech/sqlboiler/v4/boil"

	"sevensoupcans.com/backend/vent3/internal/models"

	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	"github.com/golang-jwt/jwt/v4"
	"sevensoupcans.com/backend/vent3/config"
)

var (
	//AccessibleConfig configuration object for jwtware to refresh logged in tokens for authenticated users
	//but ignore errors for non-authenticated users for accessible routes
	AccessibleConfig jwtware.Config

	//RestrictedConfig configuration object for jwtware that will return error codes if nonauthenticated
	//users try to access restricted routes
	RestrictedConfig jwtware.Config
)

func init() {
	AccessibleConfig = jwtware.Config{
		SuccessHandler: successHandler,
		ErrorHandler:   accessibleErrorHandler,
		SigningKey:     []byte(config.Conf.SigningKey),
	}
	RestrictedConfig = jwtware.Config{
		// SuccessHandler: successHandler,
		ErrorHandler: restrictedErrorHandler,
		SigningKey:   []byte(config.Conf.SigningKey),
	}
}

func accessibleErrorHandler(c *fiber.Ctx, err error) error {
	return c.Next()
}

func successHandler(c *fiber.Ctx) error {
	user := UserFromToken(c)
	if user != nil {
		if _, err := UpdateToken(c, user); err != nil {
			return err
		}
	}
	return c.Next()
}

func restrictedErrorHandler(c *fiber.Ctx, err error) error {
	if err.Error() == "Missing or malformed JWT" {
		c.Status(fiber.StatusBadRequest)
		return c.SendString("Missing or malformed JWT")
	} else {
		c.Status(fiber.StatusUnauthorized)
		return c.SendString("Invalid or expired JWT")
	}
}

//JWTAuthentication method looks for a JWT
//If it exists, queries the database to verifiy that the user
//exists and updates the timestamp (hopefully)
func JWTAuthentication(c *fiber.Ctx) error {
	// user := c.Locals("user").(*jwt.Token)
	// user := c.Locals("user")
	// fmt.Println(user)

	return c.Next()
}

//UserFromToken takes the context and parses
//out the important information from the JTW
//If it is expired, it returns nil
func UserFromToken(c *fiber.Ctx) *models.VentUser {

	user := c.Locals("user").(*jwt.Token)
	if user != nil {
		claims := user.Claims.(jwt.MapClaims)
		expiration := time.Unix(int64(claims["exp"].(float64)), 0)

		if time.Now().After(expiration) {
			return nil
		}

		username := claims["username"].(string)
		id := int(claims["id"].(float64))
		admin := claims["admin"].(bool)

		var user models.VentUser
		user.Username = username
		user.Admin = admin
		user.ID = id

		return &user
	}
	return nil
}

func generateJWT(user *models.VentUser, expiration time.Time, signingString string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = user.Username
	claims["id"] = user.ID
	claims["admin"] = user.Admin
	claims["exp"] = expiration.Unix()
	t, err := token.SignedString([]byte(signingString))
	if err != nil {
		config.Log.Err.Println("Error generating token string")
		return "", err
	}
	return t, nil
}

func GenerateRefreshToken(c *fiber.Ctx, user *models.VentUser) (string, error) {
	refreshUuid := uuid.New().String()

	// The WHERE clause is not being appended this way. I have no idea why
	// I tried several different forms and they didn't work so I settled on
	// hard coding the query. Perhaps you can find a better solution.
	// result, err := models.NewQuery(qm.SQL(fmt.Sprintf("UPDATE %s SET refresh_token = ?", models.TableNames.VentUsers), refreshUuid), models.VentUserWhere.ID.EQ(user.ID)).ExecContext(c.Context(), boil.GetContextDB())

	result, err := models.NewQuery(qm.SQL(fmt.Sprintf("UPDATE %s SET refresh_token = ? WHERE %s = ?", models.TableNames.VentUsers, models.VentUserColumns.ID), refreshUuid, user.ID)).ExecContext(c.Context(), boil.GetContextDB())

	if err != nil {
		return "", nil
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return "", err
	}
	if rows != 1 {
		return "", errors.New("could not update user record")
	}

	expiration := time.Now().Add(time.Hour * 24 * 7)
	token, err := generateJWT(user, expiration, refreshUuid)
	if err != nil {
		return "", err
	}
	return token, nil
}

func ValidateRefreshToken(c *fiber.Ctx, token string) (*models.VentUser, error) {
	type refreshToken struct {
		RefreshToken null.String `boil:refresh_token`
	}

	t, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		id := int(token.Claims.(jwt.MapClaims)["id"].(float64))
		var rt refreshToken
		if err := models.NewQuery(qm.Select("refresh_token"), qm.From(models.TableNames.VentUsers), models.VentUserWhere.ID.EQ(id)).BindG(c.Context(), &rt); err != nil {
			return nil, err
		}
		return []byte(rt.RefreshToken.String), nil
	})

	if err != nil {
		return nil, err
	}
	id := int(t.Claims.(jwt.MapClaims)["id"].(float64))
	return models.VentUsers(qm.Select("ID", "username", "visibleemail", "admin", "firstname", "lastname", "email"), models.VentUserWhere.ID.EQ(id)).OneG(c.Context())
}

//UpdateToken creates and returns a new token with a new expiration time
func UpdateToken(c *fiber.Ctx, user *models.VentUser) (string, error) {
	expiration := time.Now().Add(time.Hour * 1)
	token, err := generateJWT(user, expiration, config.Conf.SigningKey)
	if err != nil {
		return "", err
	}

	cookie := new(fiber.Cookie)
	cookie.Name = "Bearer_Token"
	cookie.Value = token
	cookie.Expires = expiration
	//cookie.Path = "/"
	cookie.Domain = ""
	cookie.SameSite = "Lax"
	c.Cookie(cookie)
	return token, nil
}

type JWTResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

func GetJWTResponse(c *fiber.Ctx, user *models.VentUser) (JWTResponse, error) {
	var resp JWTResponse

	access_token, err := UpdateToken(c, user)
	if err != nil {
		return resp, err
	}
	refresh_token, err := GenerateRefreshToken(c, user)
	if err != nil {
		return resp, err
	}
	return JWTResponse{AccessToken: access_token, RefreshToken: refresh_token}, nil
}
