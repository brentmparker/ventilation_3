package testProvider

import "github.com/jordan-wright/email"

type TestEmail struct {
	To      string
	Subject string
	Body    string
}

var TestEmailMessage TestEmail

func SendEmail(email *email.Email) error {
	TestEmailMessage.To = email.To[0]
	TestEmailMessage.Subject = email.Subject
	TestEmailMessage.Body = string(email.Text)
	return nil
}
