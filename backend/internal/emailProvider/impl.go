package emailProvider

import (
	"github.com/jordan-wright/email"
	"sevensoupcans.com/backend/vent3/config"
	"sevensoupcans.com/backend/vent3/internal/emailProvider/gmailProvider"
	"sevensoupcans.com/backend/vent3/internal/emailProvider/testProvider"
)

func createEmail(to string, subject string, body string) *email.Email {
	e := email.NewEmail()
	e.From = config.Conf.AdminEmail
	e.To = []string{to}
	e.Subject = subject
	e.Text = []byte(body)
	return e
}

func ConfigureEmailProvider() {
	var err error
	if config.Conf.EmailProvider == "gmailProvider" {
		err = gmailProvider.ConfigureGmailClient("credentials.json", "token.json")
	}
	if err != nil {
		config.Log.Warn.Println("Error configuring email provider")
		config.Log.Warn.Println(err)
	}
}

func SendEmail(to string, subject string, body string) error {
	e := createEmail(to, subject, body)
	if config.Conf.EmailProvider == "gmailProvider" {
		return gmailProvider.SendEmail(e)
	}
	if config.Conf.EmailProvider == "testProvider" {
		return testProvider.SendEmail(e)
	}
	return nil
}
