package main

import (
	"context"
	"fmt"
	"strconv"

	"sevensoupcans.com/backend/vent3/internal/emailProvider"

	"sevensoupcans.com/backend/vent3/internal/models"

	"sevensoupcans.com/backend/vent3/internal/middleware"

	"sevensoupcans.com/backend/vent3/internal/ventdb"

	"github.com/gofiber/fiber/v2"
)

func getRecentPosts(c *fiber.Ctx, offset int) error {
	posts, err := ventdb.GetRecentPosts(c.Context(), offset)
	if err != nil {
		return err
	}
	return c.JSON(posts)
}

func getJournalRoot(c *fiber.Ctx) error {
	recent := c.Query("recent")
	offset, err := strconv.Atoi(c.Query("offset", "0"))
	if err != nil {
		offset = 0
	}
	if recent == "true" {
		return getRecentPosts(c, offset)
	}
	// Get admin posts
	posts, _ := ventdb.GetJournalPosts(0, offset, 5, c.Context())
	return c.JSON(posts)
}

func getJournal(c *fiber.Ctx) error {
	jid, err := strconv.Atoi(c.Params("jid"))
	if err != nil {
		return err
	}
	//Adding this to decouple database from web server
	limit, err := strconv.Atoi(c.Query("limit"))
	if err != nil {
		limit = 5
	}
	err = nil

	offset, err := strconv.Atoi(c.Query("offset"))
	if err != nil {
		offset = 0
	}
	err = nil

	posts, err := ventdb.GetJournalPosts(jid, offset, limit, c.Context())
	if err != nil {
		return err
	}

	return c.JSON(posts)
}

type tokenResponse struct {
	*models.VentUser
	Tokens middleware.JWTResponse `json:"tokens"`
}

func postLogin(c *fiber.Ctx) error {
	creds := new(ventdb.Credentials)
	if err := c.BodyParser(creds); err != nil {
		return err
	}

	user, err := ventdb.Authenticate(c.Context(), *creds)
	if err != nil {
		// No rows found, so username or password is invalid
		// return err
		return c.Status(fiber.StatusUnauthorized).SendString("Invalid username or password")
	}
	if user != nil && user.ID > 0 {
		resp, err := middleware.GetJWTResponse(c, user)
		if err != nil {
			return err
		}
		loginResp := tokenResponse{VentUser: user, Tokens: resp}
		return c.JSON(loginResp)
	} else {
		return c.Status(fiber.StatusUnauthorized).SendString("Invalid username or password")
	}
}

func generalErrorHandler(c *fiber.Ctx, message string, err error) error {
	logger.Err.Printf(`%s %s - %s`, c.Method(), c.Path(), c.IP())
	logger.Err.Printf(message, err)
	return c.Status(fiber.StatusBadRequest).SendString(err.Error())
}

func getAdmins(c *fiber.Ctx) error {
	// List of admin users
	admins, err := ventdb.GetAdminUsers(c.Context())
	if err != nil {
		return err
	}
	return c.JSON(admins)
}

type VentModule struct {
	Name    string
	Content string
}

type StaticWrapper struct {
	Title   string
	CSSFile string
	RSSLink string
	TagLine string
	Posts   []*ventdb.PostWithDetails
	Modules []*VentModule
}

func getModules(ctx context.Context) ([]*VentModule, error) {
	var modules []*VentModule
	lyric_content, err := ventdb.GetLyric(ctx)
	if err != nil {
		return nil, err
	}
	lyric := VentModule{Name: "Lyric", Content: lyric_content.Lyric.String}
	modules = append(modules, &lyric)
	return modules, nil
}

func renderIndex(c *fiber.Ctx) error {
	posts, err := ventdb.GetJournalPosts(0, 0, 50, c.Context())
	if err != nil {
		return err
	}
	modules, err := getModules(c.Context())
	if err != nil {
		return err
	}
	wrapper := StaticWrapper{Posts: posts, Title: "Vent 3", CSSFile: "themes/test2/test2.css", TagLine: "Everything Sucks", Modules: modules}

	return c.Status(fiber.StatusOK).Render("test2/index", wrapper)
}

func getLyric(c *fiber.Ctx) error {
	lyric, err := ventdb.GetLyric(c.Context())
	if err != nil {
		return err
	}
	return c.JSON(lyric)
}

func getCurrentUser(c *fiber.Ctx) error {
	user := middleware.UserFromToken(c)
	if user != nil {
		user, err := models.VentUsers(models.VentUserWhere.ID.EQ(user.ID)).OneG(c.Context())
		if err != nil {
			return err
		}
		return c.JSON(user)
	}
	return nil
}

func getComments(c *fiber.Ctx) error {
	jid, err := strconv.Atoi(c.Params("jid"))
	if err != nil {
		return err
	}

	pid, err := strconv.Atoi(c.Params("pid"))
	if err != nil {
		return err
	}

	comments, err := ventdb.GetCommentsByPostID(jid, pid, c.Context())
	if err != nil {
		return err
	}
	return c.JSON(comments)
}

func newPost(c *fiber.Ctx) error {
	//TODO: ensure we parse and return the correctly formated post struct
	post := new(ventdb.PostWithDetails)
	// post := new(models.VentPost)
	if err := c.BodyParser(post); err != nil {
		return generalErrorHandler(c, "Error parsing post: %v", err)
	}
	user, err := getLoggedInUser(c)
	if err != nil {
		return nil
	}
	post.UserID = user.ID

	newPost, err := ventdb.InsertPost(c.Context(), post)
	if err != nil {
		return generalErrorHandler(c, "Error inserting post: %v", err)
	}

	return c.Status(fiber.StatusCreated).JSON(newPost)
}

func newComment(c *fiber.Ctx) error {
	user, err := getLoggedInUser(c)
	if err != nil {
		return nil
	}

	comment := new(ventdb.CommentWithUser)
	// TODO: Save IP
	//comment.IP.Scan(c.IP())
	if err := c.BodyParser(comment); err != nil {
		return generalErrorHandler(c, "Error parsing comment: %v", err)
	}
	comment.UserID = user.ID

	newComment, err := ventdb.InsertComment(c.Context(), comment)
	if err != nil {
		return err
	}
	return c.Status(fiber.StatusCreated).JSON(newComment)
}

func newUser(c *fiber.Ctx) error {
	rawUser := new(ventdb.RawUser)
	if err := c.BodyParser(rawUser); err != nil {
		return generalErrorHandler(c, "Error parsing new user: %v", err)
	}
	rawUser.Pending = true

	user, err := ventdb.InsertUser(c.Context(), rawUser)
	if err != nil {
		return generalErrorHandler(c, "Error inserting new user: %v", err)
	}

	if err := emailProvider.SendEmail(user.Email, "New Vent Account", fmt.Sprintf("Confirm your account by clicking here: http://localhost:5000/users/confirm/%v", user.UUID)); err != nil {
		return err
	}

	return c.Status(fiber.StatusCreated).JSON(user)
}

func getLoggedInUser(c *fiber.Ctx) (*models.VentUser, error) {
	user := middleware.UserFromToken(c)
	if user.ID == 0 {
		return nil, c.SendStatus(fiber.StatusUnauthorized)
	}
	return user, nil
}

func getActiveUsers(c *fiber.Ctx) error {
	users, err := ventdb.GetActiveUsers(c.Context())
	if err != nil {
		return err
	}

	return c.JSON(users)
}

func updatePost(c *fiber.Ctx) error {
	user, err := getLoggedInUser(c)
	if err != nil {
		return err
	}
	if user.ID == 0 {
		return nil
	}
	post := new(ventdb.PostWithDetails)
	if err := c.BodyParser(post); err != nil {
		return generalErrorHandler(c, "Error parsing post: %v", err)
	}

	existingPost, err := models.VentPosts(models.VentPostWhere.ID.EQ(post.ID)).OneG(c.Context())
	if err != nil {
		return err
	}
	if existingPost.UserID != user.ID {
		if !user.Admin {
			return c.SendStatus(fiber.StatusUnauthorized)
		}
		post.UserID = user.ID
	}

	newPost, err := ventdb.UpdatePost(c.Context(), post)
	if err != nil {
		return generalErrorHandler(c, "Error inserting post: %v", err)
	}

	return c.JSON(newPost)

}

func updateComment(c *fiber.Ctx) error {
	comment := new(models.VentComment)
	user := middleware.UserFromToken(c)
	if user == nil {
		return c.SendStatus(fiber.StatusUnauthorized)
	}

	if err := c.BodyParser(comment); err != nil {
		return generalErrorHandler(c, "Error parsing comment: %v", err)
	}
	if comment.UserID != user.ID {
		if !user.Admin {
			return c.SendStatus(fiber.StatusUnauthorized)
		}
	}

	newComment, err := ventdb.UpdateComment(c.Context(), comment)
	if err != nil {
		return generalErrorHandler(c, "%v", err)
	}

	return c.JSON(newComment)
}

func updateUser(c *fiber.Ctx) error {
	user, err := getLoggedInUser(c)
	if err != nil {
		return err
	}

	updatedUser := new(models.VentUser)
	if err := c.BodyParser(updatedUser); err != nil {
		return generalErrorHandler(c, "Error parsing user: %v", err)
	}

	if updatedUser.ID != user.ID {
		if !user.Admin {
			return c.SendStatus(fiber.StatusUnauthorized)
		}
	}

	user, err = ventdb.UpdateUser(c.Context(), updatedUser)
	if err != nil {
		return generalErrorHandler(c, "%v", err)
	}
	return c.JSON(user)
}

func updatePassword(c *fiber.Ctx) error {
	user, err := getLoggedInUser(c)
	if err != nil {
		return err
	}

	updatedUser := new(ventdb.RawUser)
	if err := c.BodyParser(updatedUser); err != nil {
		return generalErrorHandler(c, "Error parsing user: %v", err)
	}

	if updatedUser.ID != user.ID {
		if !user.Admin {
			return c.SendStatus(fiber.StatusUnauthorized)
		}
	}

	updated, err := ventdb.UpdatePassword(c.Context(), updatedUser)
	if err != nil {
		return generalErrorHandler(c, "%v", err)
	}
	return c.JSON(updated)
}

func deletePost(c *fiber.Ctx) error {
	user, err := getLoggedInUser(c)
	if err != nil {
		return err
	}

	pid, err := strconv.Atoi(c.Params("pid"))
	if err != nil {
		return generalErrorHandler(c, "Error parsing post ID: %v", err)
	}

	post := models.VentPost{ID: pid}
	if err := post.ReloadG(c.Context()); err != nil {
		return err
	}
	if post.UserID != user.ID {
		if !user.Admin {
			return c.SendStatus(fiber.StatusUnauthorized)
		}
	}
	if err := ventdb.DeletePost(c.Context(), post.ID); err != nil {
		return err
	}
	return c.SendStatus(fiber.StatusAccepted)
}

func deleteComment(c *fiber.Ctx) error {
	user, err := getLoggedInUser(c)
	if err != nil {
		return err
	}

	cid, err := strconv.Atoi(c.Params("cid"))
	if err != nil {
		return generalErrorHandler(c, "Error parsing post ID: %v", err)
	}

	comment := new(models.VentComment)
	comment.ID = cid
	if err := comment.ReloadG(c.Context()); err != nil {
		return err
	}

	if comment.UserID != user.ID {
		if !user.Admin {
			return c.SendStatus(fiber.StatusUnauthorized)
		}
	}

	if err := ventdb.DeleteComment(c.Context(), comment); err != nil {
		return err
	}

	return c.SendStatus(fiber.StatusAccepted)
}

func deleteUser(c *fiber.Ctx) error {
	return nil
}

func confirmNewUser(c *fiber.Ctx) error {

	uuid := new(string)
	if err := c.BodyParser(uuid); err != nil {
		return generalErrorHandler(c, "Error parsing uuid: %v", err)
	}

	result, err := ventdb.ConfirmNewUser(c.Context(), *uuid)
	if err != nil {
		return generalErrorHandler(c, "Error confirming new user: %v", err)
	}

	return c.JSON(result)
}

func confirmNewUserGet(c *fiber.Ctx) error {
	uuid := c.Params("uuid")
	result, err := ventdb.ConfirmNewUser(c.Context(), uuid)
	if err != nil {
		// Need to redirect to error page if no rows found.
		// That means we are unable to confirm a new user.
		// return generalErrorHandler(c, "Error confirming new user: %v", err)
		c.Status(fiber.StatusFound)
		c.Location("/error?error=confirm")
	}

	// TODO: Redirect to the UI with a nice message or something
	if result {
		c.Status(fiber.StatusFound)
		c.Location("/signin?message=user_confirmed")
	} else {
		c.Status(fiber.StatusFound)
		c.Location("/error?error=confirm")
	}

	return c.JSON(result)
}

func refreshAuthenticatedUser(c *fiber.Ctx) error {
	type refreshToken struct {
		RefreshToken string `json:"refresh_token"`
	}

	token := new(refreshToken)

	if err := c.BodyParser(token); err != nil {
		fmt.Println(err)
		return err
	}

	user, err := middleware.ValidateRefreshToken(c, token.RefreshToken)
	if err != nil {
		if err := c.Status(fiber.StatusUnauthorized).SendString("Invalid or expired JWT"); err != nil {
			return err
		}
		return err
	}

	resp, err := middleware.GetJWTResponse(c, user)
	if err != nil {
		return err
	}

	response := tokenResponse{VentUser: user, Tokens: resp}
	return c.JSON(response)
}
