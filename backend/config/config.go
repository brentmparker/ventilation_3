package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

//Config represents data coming from config file
type Config struct {
	APIBasePath     string   `json:"apiBasePath"`
	Certkey         string   `json:"sslCertKey"`
	CertPath        string   `json:"sslCertPath"`
	LogPath         string   `json:"logPath"`
	Port            int      `json:"port"`
	UseSSL          bool     `json:"useSSL"`
	ServeStatic     bool     `json:"serveStatic"`
	StaticBasePath  string   `json:"staticBasePath"`
	StaticFilesPath []string `json:"staticFilesPath"`
	DSNBase         string   `json:"dsnBase"`
	DSNSuffix       string   `json:"dsnSuffix"`
	DBDriver        string   `json:"dbdriver"`
	DBName          string   `json:"dbName"`
	SigningKey      string   `json:"signingKey"`
	AdminEmail      string   `json:"adminEmail"`
	EmailProvider   string   `json:"emailProvider"`
	LogToStdOut     bool     `json:"logToStdOut"`
}

func (c Config) GetDSN() string {
	return c.DSNBase + c.DBName + c.DSNSuffix
}

func (c Config) GetDSNURL() string {
	val := c.DBDriver + "://" + c.GetDSN()
	return val
}

func (c Config) GetStaticFilesPath() string {
	return filepath.Join(c.StaticFilesPath...)
}

//Logger contains three logging objects for info, warnings, and errors
type Logger struct {
	Info *log.Logger
	Warn *log.Logger
	Err  *log.Logger
}

var (
	//Log importable logging object to log everything to a single file
	Log *Logger

	//Conf important configuration file
	Conf *Config

	ConfigPath         = "config.json"
	ConfigOverridePath = "config.override.json"
)

func ReadConfigFile(config *Config, path string) error {
	configFile, err := os.Open(path)
	if err != nil {
		return err
	}
	byteValue, err := ioutil.ReadAll(configFile)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(byteValue, &config); err != nil {
		return err
	}
	if err := configFile.Close(); err != nil {
		return err
	}
	return nil
}

func createLogger(path string) *Logger {

	var logger Logger
	var file *os.File
	var err error
	if Conf.LogToStdOut {
		file = os.Stdout
	} else {
		file, err = os.OpenFile(Conf.LogPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			log.Fatal(err)
		}
	}
	logger.Info = log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	logger.Warn = log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	logger.Err = log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)

	return &logger
}

func ReadConfigFiles(files []string) []error {
	var errs []error

	for _, f := range files {
		if err := ReadConfigFile(Conf, f); err != nil {
			errs = append(errs, err)
			log.Print(err)
		}
	}
	return errs
}

func init() {
	path, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(path)

	Conf = new(Config)
	Conf.LogToStdOut = true

	_ = ReadConfigFiles([]string{ConfigPath, ConfigOverridePath})

	Log = createLogger(Conf.LogPath)
}
