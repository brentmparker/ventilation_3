package main

import (
	"fmt"
	"html/template"
	"os"
	"path/filepath"
	"sevensoupcans.com/backend/vent3/internal/ventdb"

	"github.com/Masterminds/sprig"

	"github.com/gofiber/fiber/v2/middleware/cors"

	"sevensoupcans.com/backend/vent3/internal/emailProvider"

	ventMiddleware "sevensoupcans.com/backend/vent3/internal/middleware"

	"sevensoupcans.com/backend/vent3/config"

	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	"github.com/gofiber/template/html"

	fiberLogger "github.com/gofiber/fiber/v2/middleware/logger"
)

var (
	conf   *config.Config
	logger *config.Logger
)

func init() {
	conf = config.Conf
	logger = config.Log
}

//DELETE, GET, PATCH --update,modify, POST --create, PUT --update,replace

func setupRoutes(routes []Route, app *fiber.App) {
	for _, route := range routes {
		switch method := route.Method; method {
		case "GET":
			app.Get(route.Path, route.Handler)
		case "POST":
			app.Post(route.Path, route.Handler)
		case "PUT":
			app.Put(route.Path, route.Handler)
		case "PATCH":
			app.Patch(route.Path, route.Handler)
		case "DELETE":
			app.Delete(route.Path, route.Handler)
		}
	}
}

func CreateTestApp() *fiber.App {
	app := fiber.New()
	setupRoutes(accessible, app)
	return app
}

func CreateApp() *fiber.App	{
	cwd, err := os.Getwd()
	if err != nil {
		logger.Err.Fatal(err)
	}
	tplPath := filepath.Join(cwd, config.Conf.GetStaticFilesPath(), "themes")
	engine := html.New(tplPath, ".html.tpl")
	engine.Reload(true)
	engine.AddFunc("unescapeHTML", func(s string) template.HTML {
		return template.HTML(s)
	})
	for k, v := range sprig.FuncMap() {
		engine.AddFunc(k, v)
	}

	app := fiber.New(fiber.Config{
		Views: engine,
	})

	app.Use(fiberLogger.New())

	//Remove for production
	app.Use(cors.New())

	app.Use(jwtware.New(ventMiddleware.AccessibleConfig))
	if conf.ServeStatic {
		app.Static(conf.StaticBasePath, config.Conf.GetStaticFilesPath(), fiber.Static{CacheDuration: -1})
	}
	app.Use(ventMiddleware.JWTAuthentication)
	setupRoutes(accessible, app)

	app.Use(jwtware.New(ventMiddleware.RestrictedConfig))
	setupRoutes(restricted, app)
	return app
}

func main() {
	logger.Info.Print("Initializing Server...")
	logger.Info.Printf("PID:  %d", os.Getpid())

	emailProvider.ConfigureEmailProvider()
	app := CreateApp()

	// Initialize database and migrate
	ventdb.InitDatabase(true)


	// setupRotues(admin, app)
	logger.Info.Printf("Listening on PORT: %v", conf.Port)

	if conf.UseSSL {
		// TODO: Fix SSL Support
		/*
			cer, err := tls.LoadX509KeyPair(conf.CertPath, conf.Certkey)
			if err != nil {
				logger.Err.Fatal(err)
			}
			webserverConfig := &tls.Config{Certificates: []tls.Certificate{cer}}
		*/

		logger.Err.Fatal(app.Listen(fmt.Sprintf(":%v", conf.Port)))
	} else {
		logger.Err.Fatal(app.Listen(fmt.Sprintf(":%v", conf.Port)))
	}
}

/*
//Represent the component as a struct
//Shared dependencies as fields
//No global state
type server struct {
	db	*ventDb
	router *router
	// Whatever else is needed
}

func newServer() *server {
	//Setup server
	//Setup routes
	//No dependencies here, no connect to database, no logger... Only set up server responsibilities
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.ServeHTTP(w, r)
}

func (s *server) handleSomething() http.HanlderFunc {
	thing: prepareThing()
	return func(w http.ResponseWriter, r *http.Request) {
		//use thing
	}
}

func (s *server) handleAuthLogin() http.HandlerFunc {

}

func (s *server) handleAuthLogout() http.HandlerFunc {

}

//Middleware
func (s *server) adminOnly(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Reqeust) {
		if !currentUser(r).IsAdmin {
			http.NotFound(w, r)
			return
		}
		h(w, r)
	}
}

func main() {
	if err := run(); err != nil {
		fmt.Printf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}

func run() error {
	//Setup here
}
*/
