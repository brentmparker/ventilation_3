module sevensoupcans.com/backend/vent3

go 1.16

require (
	github.com/Masterminds/goutils v1.1.1 // indirect
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/Masterminds/sprig v2.22.0+incompatible
	github.com/friendsofgo/errors v0.9.2
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofiber/fiber/v2 v2.20.1
	github.com/gofiber/jwt/v3 v3.1.1
	github.com/gofiber/template v1.6.17
	github.com/golang-jwt/jwt/v4 v4.1.0
	github.com/golang-migrate/migrate/v4 v4.15.0
	github.com/google/uuid v1.2.0
	github.com/huandu/xstrings v1.3.2 // indirect
	github.com/imdario/mergo v0.3.12 // indirect
	github.com/jordan-wright/email v0.0.0-20200602115436-fd8a7622303e
	github.com/mitchellh/copystructure v1.2.0 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/volatiletech/null/v8 v8.1.2
	github.com/volatiletech/sqlboiler/v4 v4.7.1
	github.com/volatiletech/strmangle v0.0.1
	golang.org/x/net v0.0.0-20210520170846-37e1c6afe023
	golang.org/x/oauth2 v0.0.0-20210819190943-2bc19b11175f
	google.golang.org/api v0.56.0
)
