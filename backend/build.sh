export GOPATH=/usr/local/go:$HOME/go:$HOME/Development/Web/ventilation_3/backend/
go build -o ~/public_html/test/vent_3_server -v
go clean -v

rm -r ~/public_html/test/public
cp -r ~/Development/Web/ventilation_3/frontend/public ~/public_html/test/public
cp ~/Development/Web/ventilation_3/backend/config.json ~/public_html/test/config.json