{{- define "vent_classic.post" -}}
<a name="{{ .ID }}"></a>
<div class="topholder">
    <div class="topleft"></div><div class="entry"></div><div class="date">{{ .CreatedAt }}</div><div class="topright"></div>
</div>
<div class="left">
    <div class="right">
        <div class="contentcontainer">
            <a href="mailto:{{ template "email" .User }}">{{ .User.Username }}</a>: {{ .Title }}
            <hr />
            {{ unescapeHTML .Content }}
            <br />
            <br />
            {{- range $v := .Details }}
            <b>{{ $v.Name }}:</b> {{ $v.Value }}<br>
            {{- end }}
            <br />
            <br />
            {{- if .Tags }}
            {{- $tags := list -}}
            {{- range $t := .Tags }}
                {{- $tags = append $tags (printf "<a href=\"\">%v</a>" $t.Tag) }}
            {{- end }}
            Tags (beta): {{ unescapeHTML (join ", " $tags) }}
            {{- end }}
            <!--vent.post.tags-->
            <div align="right">
                <a href="{{ .ID }}/comments.html">Comments ({{ .CommentCount }}) <!--vent.post.locked--></a>
            </div>
        </div>
    </div>
</div>
<div class="bottomholder">
    <div class="bottomleft"></div><div class="bottomright"></div><div class="entrybottom"></div>
</div>
{{- end -}}

{{- define "vent_classic.module" }}
    <div class="topholder">
        <div class="topleft"></div><div class="date">{{ .Name }}</div><div class="topright"></div>
    </div>
    <div class="left">
        <div class="right">
            <div class="contentcontainer">
                {{ .Content }}
            </div>
        </div>
    </div>
    <div class="bottomholder">
        <div class="bottomleft"></div><div class="bottomright"></div>
    </div>
{{- end }}

