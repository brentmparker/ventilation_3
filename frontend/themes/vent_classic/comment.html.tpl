<!--vent.post.commentstart-->
<div class="topholder">
    <div class="topleft"></div><div class="topright"></div>
</div>
<div class="left">
    <div class="right">
        <div class="contentcontainer">
            <a href="mailto:<!--vent.post.user.email-->"><!--vent.post.user.name--></a> - (<!--vent.post.user.visibleemail-->)<!--vent.post.user.designation-->
            <hr />
            <div id="comment_options"><!--vent.comment.options--></div>
            <div id="ajax_comment_<!--vent.comment.id-->">
                <!--vent.post.content-->
            </div>
            <!--vent.comment.ajax-->
        </div>
    </div>
</div>
<div class="bottomholder">
    <div class="bottomleft"></div><div class="bottomright"></div>
</div>
<!--vent.post.commentend-->
<!--vent.post.blankstart-->
<div class="topholder">
    <div class="topleft"></div><div class="topright"></div>
</div>
<div class="left">
    <div class="right">
        <div class="contentcontainer">
            <!--vent.post.content-->&nbsp;
        </div>
    </div>
</div>
<div class="bottomholder">
    <div class="bottomleft"></div><div class="bottomright"></div>
</div>
<!--vent.post.blankend-->
