<?php
$theme_name = "Vent Classic";
$theme_css = array();
$theme_css["Blue"] = "blue.css";
$theme_css["Black"] = "black.css";
$theme_css["Monochrome"] = "mono.css";
$theme_css["Red_Basic"] = "redbasic.css";
$theme_css["Strip_Club"] = "strip-club.css";
$theme_css["Nippon"] = "nippon.css";
$theme_css["Ctrl-Alt"] = "ctrl-alt.css";
$theme_css_default = "Blue";
?>