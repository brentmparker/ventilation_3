{{ define "details" }}
    {{- range $v := .Details }}
        <b>{{ $v.Name }}:</b> {{ $v.Value }}<br>
    {{- end }}
{{ end }}

{{- define "tags" }}
    {{- if .Tags }}
        {{- $tags := list -}}
        {{- range $t := .Tags }}
            {{- $tags = append $tags (printf "<a href=\"\">%v</a>" $t.Tag) }}
        {{- end }}
        Tags (beta): {{ unescapeHTML (join ", " $tags) }}
    {{- end }}
{{- end }}

{{- define "email" }}
{{- if .Email.Valid }}{{ .Email.String }}{{ end }}
{{- end }}
