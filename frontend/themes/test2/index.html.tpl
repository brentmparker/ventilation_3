<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{{ .Title }}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="themes/test2/test2.css" type="text/css" rel="stylesheet" />
<link rel="alternate" type="application/rss+xml" title="{{ .Title }}" href="<!--vent.system.rss.link-->" />
<!--vent.system.head-->
</head>
<body>
<div class="header">
	<div class="logo"></div>
	<div class="mainmenu">
		<!--vent.system.mainmenu-->
		<div class="clearer"></div>
	</div>
	<div class="tagline">
		{{ .TagLine }}
	</div>
</div>
<!--vent.system.headerend-->
<!--vent.post.markupstart-->
<div class="posts">
<!--vent.post.repeatstart-->
	{{- range $p := .Posts }}
		{{ template "test2.post" $p}}
	{{- end }}


	<div class="clearer"></div> 	
</div>
<!--vent.post.markupend-->

<!--vent.modules.markupstart-->
<div class="boxes">
<!--vent.modules.repeatstart-->
{{- if .Modules }}
	{{- range $m := .Modules }}
		{{ template "test2.module" $m}}
	{{- end }}
{{- end }}
<!--vent.modules.repeatend-->
</div>
<!--vent.modules.markupend-->
<!--vent.system.footerbegin-->
<div class="footer">
	<a href="<!--vent.system.rss.link-->"><!--vent.system.rss.name--></a><br /><br />
	<!--vent.system.footer--><br />
	Page generated in <!--vent.system.gentime--> with <!--vent.system.querycount--> database queries.
	<p>
		<a href="http://validator.w3.org/check?uri=referer"><img src="themes/vent_classic/valid-xhtml10.png" alt="Valid XHTML 1.0 Transitional" /></a>
		 <a href="http://jigsaw.w3.org/css-validator/"><img style="border:0;width:88px;height:31px" src="themes/vent_classic/vcss.png"
       alt="Valid CSS!" /></a>
		<a href="http://feedvalidator.org/check.cgi?url=http://www.sevensoupcans.com/vent2/rss.php"><img src="themes/vent_classic/valid-rss.png" alt="[Valid RSS]" title="Validate my RSS feed" /></a>
	</p>
</div>
</body>
</html>
