{{- define "test2.post" }}
<a name=".ID"></a>
<div class="entrycontainer">
    <div class="entrytop"><div class="date">{{ .CreatedAt }}</div></div>
    <div class="post">
        <div class="entry"></div>
        <div class="postcontent">
            <a href="mailto:{{ template "email" .User }}">{{ .User.Username }}</a>: {{ .Title }}
            <hr />
            {{ unescapeHTML $.Content }}
            <br />
            {{ block "details" . }}
            {{- end }}
            <br />
            {{ block "tags" . }}
            {{- end }}
            <p align="right"><a href="{{ .ID }}/comments.html">Comments ({{ .CommentCount }}) <!--vent.post.locked--></a></p>
        </div>
    </div>
    <div class="clearer"></div>
</div>
{{- end }}

{{- define "test2.comment" }}
    <div class="commentcontainer">
        <div class="post_comment">
            <a href="mailto:<!--vent.post.user.email-->"><!--vent.post.user.name--></a> - (<!--vent.post.user.visibleemail-->)<!--vent.post.user.designation-->
            <hr />
            <div id="comment_options"><!--vent.comment.options--></div>
            <div id="ajax_comment_<!--vent.comment.id-->">
                <!--vent.post.content-->
            </div>
            <!--vent.comment.ajax-->
        </div>
    </div>
    <!--vent.post.commentend-->
{{- end }}

{{- define "test2.blank" }}
<!--vent.post.blankstart-->
<div class="commentcontainer">
    <div class="post_comment">
        <!--vent.post.content-->
    </div>
</div>
<!--vent.post.blankend-->
{{- end }}

{{ define "test2.module" }}
<div class="box">
    <div class="boxtop">{{ .Name }}</div>
    <div class="boxcontent">
        {{ .Content }}
    </div>
</div>
{{- end }}
