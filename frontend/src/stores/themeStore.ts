import { writable, derived } from 'svelte/store';
import { tick } from 'svelte';
import type { SvelteComponent } from 'svelte';

import {
	AdminBox,
	Header,
	Journal,
	LyricsBox,
	Profile,
	RecentPostsBox,
	UserActionsBox
} from '$lib/modernized/Themes/Default';

// import {
// 	Header as ComicHeader,
// 	RecentPostsBox as ComicRecentPostsBox,
// 	LyricsBox as ComicLyricsBox,
// 	UserActionsBox as ComicUserActionsBox,
// 	AdminBox as ComicAdminBox,
// 	Journal as ComicJournal
// } from '$lib/modernized/Themes/Comic';

interface Theme {
	name: string;
	adminBox: new (...args: any[]) => SvelteComponent;
	header: new (...args: any[]) => SvelteComponent;
	journal: new (...args: any[]) => SvelteComponent;
	lyricsBox: new (...args: any[]) => SvelteComponent;
	profile: new (...args: any[]) => SvelteComponent;
	recentPostsBox: new (...args: any[]) => SvelteComponent;
	userActionsBox: new (...args: any[]) => SvelteComponent;
}

function createTheme() {
	const loadedThemes = {
		Default: {
			name: 'Default',
			adminBox: AdminBox,
			header: Header,
			journal: Journal,
			lyricsBox: LyricsBox,
			profile: Profile,
			recentPostsBox: RecentPostsBox,
			userActionsBox: UserActionsBox
		}
		// Comic: {
		// 	adminBox: ComicAdminBox,
		// 	header: ComicHeader,
		// 	journal: ComicJournal,
		// 	lyricsBox: ComicLyricsBox,
		// 	recentPostsBox: ComicRecentPostsBox,
		// 	userActionsBox: ComicUserActionsBox
		// }
	};

	const { subscribe, update, set } = writable<Theme>(loadedThemes['Default']);

	async function importTheme(theme: string) {
		import(/* @vite-ignore */ `../lib/modernized/Themes/${theme}`)
			.then((modules) => {
				const {
					// ActiveUser,
					AdminBox,
					// Comment,
					Header,
					Journal,
					LyricsBox,
					Profile,
					// Post,
					RecentPostsBox,
					UserActionsBox
				} = modules;
				const components: Theme = {
					name: theme,
					adminBox: AdminBox,
					header: Header,
					journal: Journal,
					lyricsBox: LyricsBox,
					profile: Profile,
					recentPostsBox: RecentPostsBox,
					userActionsBox: UserActionsBox
				};
				loadedThemes[theme] = components;
				set(components);
			})
			.catch((err) => {
				console.log('Error loading theme: ', theme, err);
				set(loadedThemes.Default);
			});
	}

	async function setTheme(theme: string) {
		// first if theme is already loaded, set it
		if (loadedThemes[theme]) {
			set(loadedThemes[theme]);
			return;
		}
		await importTheme(theme);
		await tick();
	}

	return { subscribe, setTheme };
}

const theme = createTheme();

export default theme;
