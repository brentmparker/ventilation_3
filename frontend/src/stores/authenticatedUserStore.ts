import { writable, derived, get } from 'svelte/store';
import type { iUser } from '$lib/types';
import {
	postLogin,
	refreshAuthenticatedUser,
	updateUser as _updateUser,
	deleteUser as _deleteUser,
	updatePassword
} from '$lib/api';
import { defaultUser } from '$lib/types';
import { debounce } from '$lib/utils';

const authUser = writable<iUser>({ ...defaultUser });
const authError = writable<string>(null);

const { subscribe } = derived([authUser, authError], ([$authUser, $authError]) => {
	return { user: $authUser, error: $authError };
});

function login(username: string, password: string) {
	postLogin(username, password)
		.then((user) => {
			const { tokens, ...rest } = user;
			if (tokens?.refresh_token) {
				window.localStorage.setItem('refresh_token', tokens.refresh_token);
			}
			authUser.set(rest);
			authError.set(null);
			const event = new Event('login');
			document.dispatchEvent(event);
		})
		.catch((error) => {
			authError.set(error);
		});
}

function logout() {
	disauthenticateUser();
	const event = new Event('logout');
	document.dispatchEvent(event);
}

function disauthenticateUser() {
	authUser.set({
		...defaultUser
	});
	document.cookie = 'Bearer_Token=; expires = Thu, 01 Jan 1970 00:00:00 GMT';
	window.localStorage.removeItem('refresh_token');
	window.localStorage.removeItem('expiration');
}

async function _refreshToken() {
	const refresh_token = window.localStorage.getItem('refresh_token');
	if (!refresh_token) {
		return;
	}

	const payload = { refresh_token: refresh_token };

	refreshAuthenticatedUser(payload).then((data) => {
		const { tokens, ...rest } = data;
		window.localStorage.setItem('refresh_token', tokens.refresh_token);
		authUser.set(rest);
	});
}

const refreshToken = debounce(_refreshToken, 250);

async function updateUser(user: iUser): Promise<boolean> {
	return _updateUser(user)
		.then((result) => {
			authUser.set(result);
			return true;
		})
		.catch((error) => {
			authError.set(error);
			return false;
		});
}

async function deleteUser(): Promise<boolean> {
	return _deleteUser(get(authUser))
		.then((result) => {
			if (result) {
				disauthenticateUser();
			}
			return result;
		})
		.catch((error) => {
			authError.set(error);
			return false;
		});
}

async function changePassword(user: iUser): Promise<boolean> {
	return updatePassword(user)
		.then((result) => {
			if (!result) {
				authError.set('Unable to change password');
			}
			return result;
		})
		.catch((error) => {
			authError.set(error);
			return false;
		});
}

const auth = {
	subscribe,
	changePassword,
	deleteUser,
	login,
	logout,
	refreshToken,
	updateUser
};

export default auth;
