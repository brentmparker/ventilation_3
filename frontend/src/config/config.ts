export let port = '5000';
export let protocol = 'http';
export let basePath = `${protocol}://localhost:${port}`;
