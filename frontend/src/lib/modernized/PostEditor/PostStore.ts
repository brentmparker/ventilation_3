import { writable } from 'svelte/store';
import { defaultPost } from '$lib/types';

//visible toggle this to show/hide the PostEditorDialog
export const visible = writable(false);

//editingPost This stores a copy of the the post we are editing
//It is used to trigger opening of the PostEditorDialog and to
//populate the dialog with the correct information
export const editingPost = writable({ ...defaultPost });

//savedPost This stores a copy of the post that has been created/updated
//It triggers rerendering of various parts of the UI that the new post
//may affect
export const savedPost = writable({ ...defaultPost });

//deletedPost This stores a copy of the post that has been deleted
//It triggers rerendering of various parts of the UI that the deleted
//post may affect
export const deletedPost = writable({ ...defaultPost });
