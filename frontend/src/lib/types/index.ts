import type iComment from './iComment';
import type iDetail from './iDetail';
import type iLyric from './iLyric';
import type iPost from './iPost';
import type iTag from './iTag';
import type iUser from './iUser';
import type iTokens from './iTokens';
interface iLoginResponse extends iUser {
    tokens: iTokens;
}

import { defaultComment } from './iComment';
import { defaultDetail } from './iDetail';
import { defaultLyric } from './iLyric';
import { defaultPost } from './iPost';
import { defaultTag } from './iTag';
import { defaultUser } from './iUser';

export type {
    iComment,
    iDetail,
    iLyric,
    iPost,
    iTag,
    iUser,
    iTokens,
    iLoginResponse
}

export {
    defaultComment,
    defaultDetail,
    defaultLyric,
    defaultPost,
    defaultTag,
    defaultUser,
}