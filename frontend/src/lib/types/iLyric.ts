export default interface iLyric {
    lyric: string;
}

export const defaultLyric: iLyric = {
    lyric: ''
}