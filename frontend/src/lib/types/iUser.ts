export default interface iUser {
	ID: number;
	username: string;
	email: string;
	admin: boolean;
	visibleemail?: string;
	firstname?: string;
	lastname?: string;
	created_at?: string;
	updated_at?: string;
	deleted_at?: string;
	original?: string;
	first?: string;
	second?: string;
	uuid?: string;
}

export const defaultUser: iUser = {
	ID: 0,
	username: '',
	email: '',
	admin: false,
	visibleemail: '',
	firstname: '',
	lastname: ''
};
