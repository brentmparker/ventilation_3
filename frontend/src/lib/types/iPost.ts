import type iUser from './iUser';
import type iTag from './iTag';
import type iDetail from './iDetail';
import { defaultUser } from './iUser';

export default interface iPost {
	ID: number;
	content: string;
	comment_count: number;
	created_at: string;
	title: string;
	user_id: number;
	user: iUser;
	tags?: iTag[];
	details?: iDetail[];
	[key: string]: any;
}

export const defaultPost: iPost = {
	ID: 0,
	content: '',
	comment_count: 0,
	created_at: null,
	title: '',
	user_id: 0,
	user: { ...defaultUser },
	tags: [],
	details: [],
};
