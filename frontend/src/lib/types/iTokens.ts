export default interface Tokens {
    access_token: string,
    refresh_token: string
}