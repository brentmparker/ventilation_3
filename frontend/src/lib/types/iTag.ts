export default interface iTag {
	ID: number;
	tag: string;
	post_id: number;
}

export const defaultTag: iTag = {
	ID: 0,
	tag: '',
	post_id: 0
};
