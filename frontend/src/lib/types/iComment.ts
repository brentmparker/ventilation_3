import type iUser from './iUser';
import { defaultUser } from './iUser';

export default interface iComment {
    ID: number;
    post_id: number;
    user_id: number;
    content: string;
    created_at: string;
    updated_at?: string;
    deleted_at?: string;
    [key: string]: any;
    user: iUser;
}

export const defaultComment: iComment = {
    ID: 0,
    post_id: 0,
    user_id: 0,
    content: '',
    ip: '',
    created_at: '',
    user: { ...defaultUser },
}
