export default interface iDetail {
	ID: number;
	name: string;
	value: string;
	post_id: number;
}

export const defaultDetail: iDetail = {
	ID: 0,
	post_id: 0,
	name: '',
	value: ''
};
