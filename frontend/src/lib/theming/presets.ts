import type ThemeStyle from './themestyle';

export const presets: ThemeStyle[] = [
	{
		name: 'light',
		theme: {
			'--color': '#333',
			'--alternate': '#fff',
			'--bg-color': '#fbfbfb',
			'--primary': '#1976d2',
			'--accent': '#f50057',
			'--divider': 'rgba(0,0,0,0.1)',
			'--bg-popover': '#fff',
			'--border': '#dfdfdf',
			'--label': 'rgba(0,0,0,0.3755)',
			'--bg-input-filled': 'rgba(0,0,0,0.0555)',
			'--bg-app-bar': '#888',
			'--bg-panel': '#F9F9F9',
			'--font-family': 'Roboto,Helvetica,sans-serif',
			'--heading-font-family': 'var(--font-family)',
			'--button-font-family': 'var(--font-family)',
			'--code-font-family': '"Roboto Mono",Menlo,Consolas,monospace',
			'--focus-color': 'rgba(25,118,210,0.5)',
			'--spacing': '12pt',
			'--border-radius': '4pt',
			'--box-shadow':
				'box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);'
		}
	},
	{
		name: 'dark',
		theme: {
			'--color': '#eee',
			'--alternate': '#000',
			'--bg-color': '#303134',
			'--primary': '#3ea6ff',
			'--accent': '#ff6fab',
			'--divider': 'rgba(255,255,255,0.175)',
			'--bg-popover': '#3f3f3f',
			'--border': '#555',
			'--label': 'rgba(255,255,255,0.5)',
			'--bg-input-filled': 'rgba(255,255,255,0.1)',
			'--bg-app-bar': '#838383',
			'--bg-panel': '#434343',
			'--focus-color': 'rgba(62, 166, 255, 0.5)'
		}
	},
	{
		name: 'retro',
		theme: {
			'--color': '#3c3820',
			'--alternate': '#ccc',
			'--bg-color': '#bad1b0',
			'--primary': '#716d51',
			'--accent': '#d8992c',
			'--divider': 'rgba(0,0,0,0.1)',
			'--border': '#716d51',
			'--bg-panel': '#eff0d5',
			'--label': 'rgba(0,0,0,0.3755)'
		}
	}
];
