export default interface ThemeStyle {
    name: string;
    theme: {
        [key: string]: string
    }
}