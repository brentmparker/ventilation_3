//https://developers.google.com/web/updates/2015/03/introduction-to-fetch

import { basePath } from '$config/config';
import type { iComment, iDetail, iLyric, iPost, iUser, iTag, iLoginResponse } from '../types';
import { defaultUser } from '../types';
// import { authenticatedUser } from '$stores/authenticatedUserStore';
import auth from '$stores/authenticatedUserStore';

import { get as getStore } from 'svelte/store';

const defaultHeaders: Record<string, string> = {
	Accept: 'application/json',
	'Content-Type': 'application/json'
};

function parseCookie(cookie: string) {
	if (!cookie) {
		return {};
	}
	return cookie
		.split(';')
		.map((v) => v.split('='))
		.reduce((acc, v) => {
			acc[decodeURIComponent(v[0].trim())] = decodeURIComponent(v[1].trim());
			return acc;
		}, {});
}

function delay<T>(ms: number, result?: T): Promise<T> {
	return new Promise<T>((resolve) => setTimeout(() => resolve(result), ms));
}

export async function status(response: Response): Promise<any> {
	if (response.status >= 200 && response.status < 300) {
		return Promise.resolve(response);
	} else {
		let text = await response.text();
		return Promise.reject(new Error(text || response.statusText));
	}
}

async function api<T>(url: string, init?: RequestInit): Promise<T> {
	console.log(url);

	/**
	 * Four cases:
	 * 	Have cookie/bearer_token and user in store
	 * 		then request goes through as normal
	 * 	Have cookie/bearer_token but no user in store
	 * 		app must have been refreshed so
	 * 		need to get user with refresh token
	 * 	Have no cookie but have refresh token
	 * 		Log user back in using refresh token
	 * 	No cookie and no refresh token
	 * 		User is very much logged out
	 */
	//Look through headers to set bearer token
	const access_token = parseCookie(document.cookie)['Bearer_Token'];
	const user = getStore(auth).user;
	const refresh_token = window.localStorage.getItem('refresh_token');

	// Have access token, so set authorization header
	if (access_token) {
		if (!init) {
			init = { method: 'GET', headers: {} };
		}
		init.headers['Authorization'] = `Bearer ${access_token}`;
	}

	//Have refresh_token, but access_token is missing or user is missing
	if (refresh_token && (!access_token || (access_token && user?.ID === 0))) {
		auth.refreshToken();
	}

	return fetch(url, init)
		.then(status)
		.then((response: Response) => {
			if (init.method == 'DELETE') {
				return new Promise<any>((resolve, reject) => {
					resolve(true);
				});
			}
			return response.json() as Promise<T>;
		});
	// .then( (data: T) => {return delay(1000, data)})
	// .then( (data: T) => {
	//     return new Promise<T>( () => {setTimeout(() => data, 5000)})
	// })
}

async function get<REQ, RES>(url: string, data?: REQ): Promise<RES> {
	let init = {
		method: 'GET',
		headers: {},
		body: null
	};
	if (data) {
		init.body = JSON.stringify(data);
	}
	return api(url, init);
}

async function post<REQ, RES>(url: string, data: REQ): Promise<RES> {
	let init = {
		method: 'POST',
		headers: { ...defaultHeaders },
		body: JSON.stringify(data)
	};
	return api(url, init);
}

async function put<REQ, RES>(url: string, data: REQ): Promise<RES> {
	let init = {
		method: 'PUT',
		headers: { ...defaultHeaders },
		body: JSON.stringify(data)
	};
	return api(url, init);
}

async function patch<REQ, RES>(url: string, data: REQ): Promise<RES> {
	let init = {
		method: 'PATCH',
		headers: { ...defaultHeaders },
		body: JSON.stringify(data)
	};
	return api(url, init);
}

async function del<REQ, RES>(url: string, data: REQ): Promise<RES> {
	let init = {
		method: 'DELETE',
		headers: { ...defaultHeaders },
		body: JSON.stringify(data)
	};
	return api(url, init);
}

export async function postLogin(username: string, password: string): Promise<iLoginResponse> {
	// return post<iLoginResponse>(`${basePath}/api/v1/login`, {
	// 	username: username,
	// 	password: password
	// });

	const payload = { username: username, password: password };

	let init = {
		method: 'POST',
		headers: { ...defaultHeaders },
		body: JSON.stringify(payload)
	};

	return fetch(`${basePath}/api/v1/login`, init)
		.then(status)
		.then((response: Response) => {
			return response.json() as Promise<iLoginResponse>;
		});
}

export async function getJournal(
	user_id: number,
	offset: number = 0,
	limit: number = 5
): Promise<iPost[]> {
	return get<void, iPost[]>(
		`${basePath}/api/v1/journals/${user_id}?offset=${offset}&limit=${limit}`
	);
}

export async function getAdminUsers(): Promise<iUser[]> {
	return get<void, iUser[]>(`${basePath}/api/v1/admins`);
}

export async function getLyric(): Promise<iLyric> {
	return get<void, iLyric>(`${basePath}/api/v1/lyric`);
}

export async function getComments(jid: number, pid: number): Promise<iComment[]> {
	return get<void, iComment[]>(`${basePath}/api/v1/journals/${jid}/posts/${pid}/comments`);
}

export async function getRecentPosts(recent: boolean = true, offset: number = 0): Promise<iPost[]> {
	return get<void, iPost[]>(`${basePath}/api/v1/journals?recent=${recent}&offset=${offset}`);
}

export async function newPost(nPost: iPost): Promise<iPost> {
	return post<iPost, iPost>(`${basePath}/api/v1/journals/${nPost.user.ID}/posts`, nPost);
}

export async function updatePost(nPost: iPost): Promise<iPost> {
	return patch<iPost, iPost>(
		`${basePath}/api/v1/journals/${nPost.user.ID}/posts/${nPost.ID}`,
		nPost
	);
}

export async function deletePost(nPost: iPost): Promise<boolean> {
	return del<iPost, boolean>(
		`${basePath}/api/v1/journals/${nPost.user_id}/posts/${nPost.ID}`,
		nPost
	);
}

export async function newComment(nComment: iComment): Promise<iComment> {
	return post<iComment, iComment>(
		`${basePath}/api/v1/journals/${nComment.user.ID}/posts/${nComment.post_id}/comments`,
		nComment
	);
}

export async function updateComment(nComment: iComment): Promise<iComment> {
	return patch<iComment, iComment>(
		`${basePath}/api/v1/journals/${nComment.user_id}/posts/${nComment.post_id}/comments/${nComment.ID}`,
		nComment
	);
}

export async function deleteComment(nComment: iComment): Promise<boolean> {
	return del<iComment, boolean>(
		`${basePath}/api/v1/journals/${nComment.user_id}/posts/${nComment.post_id}/comments/${nComment.ID}`,
		nComment
	);
}

// export async function getAuthenticatedUser(nUser: iUser): Promise<iUser> {
//     return get<iUser>(`${basePath}/api/v1/users`, nUser);
// }

export async function newUser(nUser: iUser): Promise<iUser> {
	return post<iUser, iUser>(`${basePath}/api/v1/journals`, nUser);
}

export async function updateUser(nUser: iUser): Promise<iUser> {
	return patch<iUser, iUser>(`${basePath}/api/v1/journals/${nUser.ID}`, nUser);
}

export async function updatePassword(nUser: iUser): Promise<boolean> {
	return patch<iUser, boolean>(`${basePath}/api/v1/journals/${nUser.ID}/pwupdate`, nUser);
}

export async function deleteUser(nUser: iUser): Promise<boolean> {
	return del<iUser, boolean>(`${basePath}/api/v1/journals/${nUser.ID}`, nUser);
}

export async function getActiveUsers(): Promise<any> {
	return get<void, any>(`${basePath}/api/v1/users`);
}

export async function exists(value: string, field: string): Promise<boolean> {
	return post<string, boolean>(`${basePath}/api/v1/users/exists?field=${field}`, value);
}

export async function signup(user: iUser): Promise<iUser> {
	return post<iUser, iUser>(`${basePath}/api/v1/users/`, user);
}

export async function confirmNewUser(uuid: string): Promise<string> {
	return post<string, string>(`${basePath}/api/v1/users/confirm`, uuid);
}

export async function refreshAuthenticatedUser(payload): Promise<iLoginResponse> {
	let init = {
		method: 'POST',
		headers: { ...defaultHeaders },
		body: JSON.stringify(payload)
	};

	return fetch(`${basePath}/api/v1/refresh`, init)
		.then(status)
		.then((response: Response) => {
			return response.json() as Promise<iLoginResponse>;
		})
		.catch((error) => {
			document.cookie = 'Bearer_Token=; expires = Thu, 01 Jan 1970 00:00:00 GMT';
			return null;
		});
}
